/**
 * Config file all settings typedef.
 */
import { GameSettingsReels } from './GameSettingsReels';
import { SettingsWinlines } from './SettingsWinlines';
import { Settings } from '@roxorgaming/bolt-slots-shared';

export type GameSettings = Settings & {
	readonly reels: GameSettingsReels;
	readonly winlines: SettingsWinlines;
};
