/**
 * Config file reels settings typedef.
 */
import { SettingsReels } from '@roxorgaming/bolt-slots-shared';
export { TimingsBounceOut, TimingsBounceInOut } from '@roxorgaming/bolt-slots-shared';

export type GameSettingsReels = SettingsReels & {
	readonly numberOfReels: number;
	readonly numberOfRows: number;
	readonly reelsDividerWidth: number;
};
