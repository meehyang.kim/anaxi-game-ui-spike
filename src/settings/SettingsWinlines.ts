/**
 * Config file winlines settings typedef.
 */

import { IGraphicsLineLayer } from '@roxorgaming/bolt-slots-reels';
import { IWinBoxConfig } from '@roxorgaming/bolt-slots-reels';

export type SettingsWinlines = {
	readonly positions: number[][];
	readonly showAllDuration: number;
	readonly cycleDuration: number;
	readonly base: IGraphicsLineLayer;
	readonly top: IGraphicsLineLayer;
	readonly winBox: IWinBoxConfig;
	readonly textFontSize: number;
	readonly symbolLineColors: [string, string][];
	readonly scatterAllWinsClearBoxWindow: boolean;
	readonly scatterAllWinsShowBox: boolean;
	readonly scatterIterateClearBoxWindow: boolean;
	readonly scatterIterateShowBox: boolean;
};
