import { AudioManager, AudioCreator, AudioPlayer, AudioDataParser } from '@roxorgaming/bolt-audio-library';
import { AssetManager } from '@roxorgaming/bolt-assets';
import { BoltTime } from '@roxorgaming/bolt-utils';
import { Howl, Howler } from 'howler';

export function createAudioManager(assetManager: AssetManager, boltTime: BoltTime): AudioManager<typeof Howler> {
	Howler.mute(false); // Forces Howler to call setupAudioContext()
	Howler.volume(1);

	return new AudioManager(Howler, new AudioCreator(Howl, assetManager), new AudioPlayer(boltTime), new AudioDataParser());
}
