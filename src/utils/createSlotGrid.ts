/**
 * This can be used for creating slot grid positions
 */
import { IPoint } from '@roxorgaming/bolt-display';
import { GridCoord, createGrid } from '@roxorgaming/bolt-slots-reels';
import '@roxorgaming/types-gcore';
import { GameSettingsReels } from '../settings';

export function createSlotGrid(reelsSettings: GameSettingsReels): IPoint[][] {
	const scale = window.gcore.display.bucket.getScale();
	const baseSymbolWidth: number = reelsSettings.baseSymbolWidth * scale;
	const baseSymbolHeight: number = reelsSettings.baseSymbolHeight * scale;
	return createGrid({
		numberReels: reelsSettings.numberOfReels,
		numberSymbols: reelsSettings.numberOfRows + reelsSettings.boundarySymbolsTop + reelsSettings.boundarySymbolsBottom,
		baseSymbolSize: { width: baseSymbolWidth, height: baseSymbolHeight },
		anchor: {
			position: { x: 0, y: 0 },
			row: GridCoord.Center,
			column: GridCoord.Center,
		},
		padding: {
			width: reelsSettings.reelsDividerWidth * scale,
			height: 0,
		},
	});
}
