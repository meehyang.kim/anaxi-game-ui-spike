/**
 * Handles any event that would result in the game being paused/resumed.
 * If paused then autoPlay is stopped.
 * Sounds and gameUpdate loop are paused/resumed.
 */

import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
import { IStateHandler, ITicker, StateHandler, StateHandlerEvents, Ticker } from '@roxorgaming/bolt-utils';
import { GameBridgeEventNames, IGSCommon } from '@roxorgaming/types-rgp-common';
import { IGCore } from '@roxorgaming/types-gcore';
import { Injector } from '@roxorgaming/bolt-injector';
import { SlotLayoutEvents } from '@roxorgaming/bolt-loading-sequence';
import { AudioManager } from '@roxorgaming/bolt-audio-library';
import { Howler } from 'howler';

export class PauseGameHandler {
	protected readonly gcore: IGCore;
	protected readonly common: IGSCommon;
	protected readonly messageBroker: IMessageBroker;
	protected readonly injector: Injector;
	protected readonly ticker: ITicker;
	protected readonly audioManager: AudioManager<typeof Howler>;

	protected readonly orientationBlocker: IStateHandler<boolean>;
	protected readonly resizeBlocker: IStateHandler<boolean>;
	protected readonly focusOff: IStateHandler<boolean>;
	protected readonly paused: IStateHandler<boolean>;
	protected readonly supportsPortrait: boolean;
	protected readonly supportsLandscape: boolean;

	constructor(messageBroker: IMessageBroker, injector: Injector) {
		this.messageBroker = messageBroker;
		this.injector = injector;

		this.gcore = window.gcore;
		this.common = this.gcore.getCommon();
		this.ticker = this.injector.get(Ticker);
		this.audioManager = this.injector.get(AudioManager);

		this.supportsPortrait = this.common.gameProperties.portrait;
		this.supportsLandscape = this.common.gameProperties.landscape;
		this.orientationBlocker = new StateHandler<boolean>(false);
		this.resizeBlocker = new StateHandler<boolean>(false);
		this.focusOff = new StateHandler<boolean>(false);
		this.paused = new StateHandler<boolean>(false);

		this.orientationBlocker.events.on(StateHandlerEvents.changed, this.recalculateIsPaused, this);
		this.resizeBlocker.events.on(StateHandlerEvents.changed, this.recalculateIsPaused, this);
		this.focusOff.events.on(StateHandlerEvents.changed, this.recalculateIsPaused, this);
		this.paused.events.on(StateHandlerEvents.changed, this.onPausedChanged, this);

		this.refreshOrientationBlocker();

		this.messageBroker.getTopic(GameBridgeEventNames.FOCUS_ON).subscribe(this.onFocusOnEvent.bind(this), PauseGameHandler.name);
		this.messageBroker.getTopic(GameBridgeEventNames.FOCUS_OFF).subscribe(this.onFocusOffEvent.bind(this), PauseGameHandler.name);
		this.messageBroker.getTopic(GameBridgeEventNames.RESIZE_BLOCKER_ON).subscribe(this.onResizeBlockerOnEvent.bind(this), PauseGameHandler.name);
		this.messageBroker.getTopic(GameBridgeEventNames.RESIZE_BLOCKER_OFF).subscribe(this.onResizeBlockerOffEvent.bind(this), PauseGameHandler.name);
		this.messageBroker.getTopic(GameBridgeEventNames.ORIENTATION_CHANGE).subscribe(this.onOrienationChangeEvent.bind(this), PauseGameHandler.name);
	}

	protected async onFocusOnEvent(): Promise<void> {
		this.focusOff.state = false;
	}

	protected async onFocusOffEvent(): Promise<void> {
		this.focusOff.state = true;
	}

	protected async onResizeBlockerOnEvent(): Promise<void> {
		this.resizeBlocker.state = true;
	}

	protected async onResizeBlockerOffEvent(): Promise<void> {
		this.resizeBlocker.state = false;
	}

	protected async onOrienationChangeEvent(): Promise<void> {
		this.refreshOrientationBlocker();
	}

	protected refreshOrientationBlocker(): void {
		const isPortrait: boolean = this.common.deviceInfo.orientation === this.common.deviceInfo.PORTRAIT;
		const isLandscape: boolean = this.common.deviceInfo.orientation === this.common.deviceInfo.LANDSCAPE;

		this.orientationBlocker.state = (isLandscape && !this.supportsLandscape) || (isPortrait && !this.supportsPortrait);
	}

	protected recalculateIsPaused(): void {
		this.paused.state = this.orientationBlocker.state || this.resizeBlocker.state || this.focusOff.state;
	}

	protected onPausedChanged(): void {
		if (this.paused.state) {
			this.ticker.stop();
			this.messageBroker.getTopic(SlotLayoutEvents.AUTO_PLAY_STOP).publishAsync();
			this.audioManager.pauseSounds();
		} else {
			this.ticker.start();
			this.audioManager.resumeSounds();
		}
	}
}
