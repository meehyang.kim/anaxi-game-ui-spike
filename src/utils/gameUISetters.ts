import { IInjector } from '@roxorgaming/bolt-injector';
import { SlotGameUI } from '@roxorgaming/bolt-loading-sequence';
import { getCurrency } from '@roxorgaming/bolt-utils';
import { BoltLocaliser } from '@roxorgaming/bolt-localisation';

/**
 * Will be called whenever spin button is available to press.
 * Updates message bar with the appropriate call to action string.
 */
export function setSpinReady(injector: IInjector): void {
	const gameUI = injector.get(SlotGameUI);
	const localiser = injector.get(BoltLocaliser);
	gameUI.layout.text.message.set(localiser.getLocalisedText({ key: 'PRESS_SPIN_TO_PLAY!' }));
}

/**
 * Will be called whenever a 'line' win is iterated over.
 * Updates message bar message string with appropriate win description.
 */
export function setLineWinText(injector: IInjector, win: number, index: number): void {
	const gameUI = injector.get(SlotGameUI);
	const formattedWin = getCurrency(win, {
		useSubUnits: true,
		splitThousands: true,
		addTrailingZeros: false,
	});

	const key = 'LINE_{LINE}_WINS_{WINAMOUNT}';
	const substitutions: { [key: string]: string }[] = [{ winAmount: formattedWin }, { line: String(index + 1) }];

	const message = injector.get(BoltLocaliser).getLocalisedText({ key, substitutions });
	gameUI.layout.text.message.set(message);
}

/**
 * Will be called whenever a 'scatter' win is iterated over.
 * Updates message bar message string with appropriate win description.
 */
export function setScatterWinText(injector: IInjector, win: number): void {
	const gameUI = injector.get(SlotGameUI);
	const formattedWin = getCurrency(win, {
		useSubUnits: true,
		splitThousands: true,
		addTrailingZeros: false,
	});

	const key = 'SCATTER_PAYS_{VALUE}';
	const substitutions: { [key: string]: string }[] = [{ value: formattedWin }];

	const message = injector.get(BoltLocaliser).getLocalisedText({ key, substitutions });
	gameUI.layout.text.message.set(message);
}
