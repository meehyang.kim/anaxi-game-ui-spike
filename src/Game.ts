/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/architecture/
 *
 * The Game.ts file is where you setup all of your injectables and dependencies
 * used throughout the game.
 *
 * The architecture is loosely defined as
 * - the View side (left side)
 * - the Command side (right side)
 *
 */
import { AssetManager } from '@roxorgaming/bolt-assets';
import { AudioManager } from '@roxorgaming/bolt-audio-library';
import { CommunicationController } from '@roxorgaming/bolt-communication';
import { AnimationManager, PixiApplicationHandler, LayoutManager } from '@roxorgaming/bolt-display';
import { Injector } from '@roxorgaming/bolt-injector';
import { BoltLocaliser, CLS2Localiser } from '@roxorgaming/bolt-localisation';
import { GameUI, SlotGameUI, GameUITopics, SlotLayoutEvents } from '@roxorgaming/bolt-loading-sequence';
import { MessageBroker } from '@roxorgaming/bolt-message-broker';
import { WritableStore, ReadableStore } from '@roxorgaming/bolt-store';
import { BoltTime, MinWagerTimeout, Ticker } from '@roxorgaming/bolt-utils';
import { Howler } from 'howler';
import { GameController } from './controller/GameController';
import { model, reducer, GameStoreModel } from './store';
import { bucketSizes } from './constants';
import { getRuntimeMap } from './view/getRuntimeMap';
import { GameResponseSCJson, GameSlotResponse } from './comms/model';
import { createAudioManager } from './utils';
import { SoundController } from './sounds/SoundController';
import { GameBridgeEventNames } from '@roxorgaming/types-rgp-common';
import { Ticker as PixiTicker } from 'pixi.js';
import { LoadingSequenceTopics } from '@roxorgaming/bolt-loading-sequence';
import { createStandardHooks, HookCallback } from '@roxorgaming/bolt-test-extras';
import {
	ENDED_WIN_CYCLE,
	ENDED_WINS_ALL,
	GAME_LOADED,
	INTRO_SCREEN_SOUND_BUTTON_CLICKED,
	LAYOUT_ADD_LAYOUT,
	LAYOUT_REMOVE_LAYOUT,
	LAYOUT_SET_LAYOUT,
	LAYOUT_SET_LAYOUT_VISIBLE,
	LOADING_SEQUENCE_RENDERING_STARTED,
	LOADING_SEQUENCE_GAME_UI_INITIALISED,
	LOADING_SET_PROGRESS,
	LAYOUT_STATES_CHANGED,
	LAYOUT_SUBSTATE_CHANGED,
	REQUEST_HANDSHAKE,
	REQUEST_HANDSHAKE_SUCCESS,
	SET_INIT_REELS,
	SET_REEL_MAPS,
	SHOW_WIN_CYCLE,
	SHOW_WIN_ITERATION,
	SHOW_WINLINE,
	SHOW_WINLINES,
	SHOW_WINS_ALL,
	SPIN_REELS,
	TRIGGER_FREE_SPIN_QUEUE,
	TRIGGER_SPIN_QUEUE,
} from '@roxorgaming/bolt-slots-shared';

export class Game {
	private readonly messageBroker: MessageBroker;
	private readonly commandInjector: Injector;
	private readonly viewInjector: Injector;
	private readonly store: WritableStore<GameStoreModel>;
	private readonly gameUI: SlotGameUI;
	private readonly animationManager: AnimationManager;
	private readonly boltTime: BoltTime;
	private readonly ticker: Ticker;
	private readonly localiser: CLS2Localiser;
	private readonly assetManager: AssetManager;
	private readonly audioManager: AudioManager<typeof Howler>;
	private readonly gameController: GameController;
	private readonly pixiApplicationHandler: PixiApplicationHandler;
	private readonly soundController: SoundController;
	private readonly minWagerTimeout: MinWagerTimeout;

	/**
	 * Here we instantiate all of our injectables and dependencies
	 */
	constructor() {
		if (DEBUG) {
			import('pixi.js').then((PIXI) => ((window as any).PIXI = PIXI));
		}

		window.gcore.display.bucket.setBucket(bucketSizes);
		this.boltTime = new BoltTime();
		this.ticker = new Ticker(PixiTicker.shared);
		this.messageBroker = new MessageBroker(this.boltTime);
		this.commandInjector = new Injector();
		this.viewInjector = new Injector();
		this.store = new WritableStore(model, reducer);
		this.gameUI = new SlotGameUI();
		this.animationManager = new AnimationManager();
		this.localiser = new CLS2Localiser();
		this.pixiApplicationHandler = new PixiApplicationHandler();
		this.assetManager = new AssetManager((progress: number) => {
			this.messageBroker.getTopic(LOADING_SET_PROGRESS).publishAsync({ topic: LOADING_SET_PROGRESS, data: progress });
		});
		this.audioManager = createAudioManager(this.assetManager, this.boltTime);
		this.gameController = new GameController(this.commandInjector, this.messageBroker);
		this.soundController = new SoundController(this.audioManager.audioPlayer);
		this.minWagerTimeout = new MinWagerTimeout(window.gcore.regulations.minTimeBetweenWagers || 0, DEBUG ? this.boltTime : undefined);
	}

	public start = async (): Promise<void> => {
		this.createMessageTopics();
		this.soundController.init();
		this.setupCommandInjector();
		this.setupViewInjector();
		new LayoutManager(this.messageBroker, this.viewInjector, getRuntimeMap());
		if (DEBUG && FAKE_BE) {
			const { initFakeBackend } = await import('./fakeBackend/initFakeBackend');
			await initFakeBackend(this.commandInjector, this.messageBroker);
		}
		this.addHooks();
		this.gameController.init();
	};

	protected addHooks(customHooks?: Map<string, HookCallback>): void {
		const hooks = createStandardHooks(this.messageBroker);
		if (customHooks) {
			customHooks.forEach((value, key) => hooks.addHook(key, value));
		}
		hooks.exposeHooks(this.commandInjector);
	}

	/**
	 * Registers all injectables used for the Command side injector
	 */
	private setupCommandInjector(): void {
		this.commandInjector.registerInstance(WritableStore, this.store);
		this.commandInjector.registerInstance(AssetManager, this.assetManager);
		this.commandInjector.registerInstance(BoltLocaliser, this.localiser);
		this.commandInjector.registerInstance(CommunicationController, new CommunicationController<string, GameResponseSCJson, GameSlotResponse>());
		this.commandInjector.registerInstance(GameUI, this.gameUI);
		this.commandInjector.registerInstance(SlotGameUI, this.gameUI);
		this.commandInjector.registerInstance(AnimationManager, this.animationManager);
		this.commandInjector.registerInstance(BoltTime, this.boltTime);
		this.commandInjector.registerInstance(Ticker, this.ticker);
		this.commandInjector.registerInstance(PixiApplicationHandler, this.pixiApplicationHandler);
		this.commandInjector.registerInstance(AudioManager, this.audioManager);
		this.commandInjector.registerInstance(SoundController, this.soundController);
		this.commandInjector.registerInstance(MinWagerTimeout, this.minWagerTimeout);
	}

	/**
	 * Registers all injectables used for the View side injector
	 */
	private setupViewInjector(): void {
		this.viewInjector.registerInstance<ReadableStore<GameStoreModel>>(ReadableStore, this.store);
		this.viewInjector.registerInstance(AssetManager, this.assetManager);
		this.viewInjector.registerInstance(BoltLocaliser, this.localiser);
		this.viewInjector.registerInstance(GameUI, this.gameUI);
		this.viewInjector.registerInstance(SlotGameUI, this.gameUI);
		this.viewInjector.registerInstance(AnimationManager, this.animationManager);
		this.viewInjector.registerInstance(BoltTime, this.boltTime);
		this.viewInjector.registerInstance(Ticker, this.ticker);
		this.viewInjector.registerInstance(PixiApplicationHandler, this.pixiApplicationHandler);
		this.viewInjector.registerInstance(AudioManager, this.audioManager);
		this.viewInjector.registerInstance(SoundController, this.soundController);
	}

	/**
	 * All Topics used in your game should be created here.
	 * You must refrain from creating topics outside of the Game.ts file as this
	 * will make future debugging harder.
	 */
	private createMessageTopics(): void {
		this.messageBroker.createTopic(LOADING_SEQUENCE_RENDERING_STARTED);
		this.messageBroker.createTopic(LOADING_SEQUENCE_GAME_UI_INITIALISED);
		this.messageBroker.createTopic(LAYOUT_STATES_CHANGED);
		this.messageBroker.createTopic(LAYOUT_SUBSTATE_CHANGED);
		this.messageBroker.createTopic(GAME_LOADED);
		this.messageBroker.createTopic(LAYOUT_ADD_LAYOUT);
		this.messageBroker.createTopic(LAYOUT_SET_LAYOUT);
		this.messageBroker.createTopic(LAYOUT_REMOVE_LAYOUT);
		this.messageBroker.createTopic(LAYOUT_SET_LAYOUT_VISIBLE);
		this.messageBroker.createTopic(LOADING_SET_PROGRESS);
		this.messageBroker.createTopic(SPIN_REELS, 0);
		this.messageBroker.createTopic(REQUEST_HANDSHAKE, 0);
		this.messageBroker.createTopic(INTRO_SCREEN_SOUND_BUTTON_CLICKED, 0);
		this.messageBroker.createTopic(REQUEST_HANDSHAKE_SUCCESS, 0);
		this.messageBroker.createTopic(TRIGGER_SPIN_QUEUE, 0);
		this.messageBroker.createTopic(TRIGGER_FREE_SPIN_QUEUE, 0);
		this.messageBroker.createTopic(SHOW_WINS_ALL);
		this.messageBroker.createTopic(SHOW_WIN_ITERATION);
		this.messageBroker.createTopic(SET_REEL_MAPS, 0);
		this.messageBroker.createTopic(SET_INIT_REELS, 0);
		this.messageBroker.createTopic(SHOW_WINLINES);
		this.messageBroker.createTopic(ENDED_WINS_ALL);
		this.messageBroker.createTopic(SHOW_WIN_CYCLE, 0);
		this.messageBroker.createTopic(SHOW_WINLINE);
		this.messageBroker.createTopic(ENDED_WIN_CYCLE);

		this.messageBroker.createTopic(LoadingSequenceTopics.CONFIGS_LOADED, 0);
		this.messageBroker.createTopic(LoadingSequenceTopics.COMMS_INITIALISED, 0);
		this.messageBroker.createTopic(LoadingSequenceTopics.GAME_LOADED_WITH_SOUND_PREF, 0);

		this.messageBroker.createTopic(SlotLayoutEvents.NEW_COIN_SIZE);
		this.messageBroker.createTopic(SlotLayoutEvents.NEW_BET_LINE);
		this.messageBroker.createTopic(SlotLayoutEvents.AUTO_PLAY_STOP);

		this.messageBroker.createTopic(GameUITopics.REQUEST_SPIN, 0);
		this.messageBroker.createTopic(GameUITopics.SKIP, 0);

		this.messageBroker.createTopic(GameBridgeEventNames.ORIENTATION_CHANGE);
		this.messageBroker.createTopic(GameBridgeEventNames.RESIZE_BLOCKER_ON);
		this.messageBroker.createTopic(GameBridgeEventNames.RESIZE_BLOCKER_OFF);
		this.messageBroker.createTopic(GameBridgeEventNames.FOCUS_OFF);
		this.messageBroker.createTopic(GameBridgeEventNames.FOCUS_ON);
		this.messageBroker.createTopic(GameBridgeEventNames.PLAY_INTERRUPTED);
	}
}
