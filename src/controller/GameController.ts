/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/wrapper/common/game-bridge/
 *
 * The GameController is responsible initialising the CommandController
 * and kick starting the loadingSequence, setup GameBridge and GameUI events handling
 */

import { Injector } from '@roxorgaming/bolt-injector';
import { IMessage, MessageBroker } from '@roxorgaming/bolt-message-broker';
import { PixiApplicationHandler } from '@roxorgaming/bolt-display';
import { AssetManager } from '@roxorgaming/bolt-assets';
import { buildCommandController, initGameComms } from '../command';
import { bucketSizes, LayoutIntro } from '../constants';
import {
	GAME_LOADED,
	LAYOUT_SET_LAYOUT,
	LOADING_SEQUENCE_GAME_UI_INITIALISED,
	LOADING_SEQUENCE_RENDERING_STARTED,
	LAYOUT_STATES_CHANGED,
	LAYOUT_SUBSTATE_CHANGED,
	REQUEST_HANDSHAKE,
} from '@roxorgaming/bolt-slots-shared';
import {
	GameBridgeHandlers,
	SlotGameUIHandlers,
	initLoadingSequence,
	GameUIEventHandler,
	SlotLayoutEvents,
	GameBridgeEventHandler,
	GameUI,
	SlotGameUI,
} from '@roxorgaming/bolt-loading-sequence';
import { StoreKeys, requestAutoSpinTimeOut, IOrientationChangeData } from '@roxorgaming/bolt-slots-shared';
import { ICoinSizeData } from '@roxorgaming/types-game-ui';
import { GameBridgeEventNames } from '@roxorgaming/types-rgp-common';
import { GameStoreModel } from '../store/types';
import { GamePaddingHandler } from '@roxorgaming/bolt-utils';
import { PauseGameHandler } from '../utils';
import { WritableStore } from '@roxorgaming/bolt-store';
import { IGCoreBucketPositionData } from '@roxorgaming/types-gcore';
import { CanvasRenderer } from '@pixi/canvas-renderer';
import { applyCanvasMixin, Stage } from '@pixi/layers';
applyCanvasMixin(CanvasRenderer);

export class GameController {
	protected readonly commandInjector: Injector;
	protected readonly messageBroker: MessageBroker;
	protected readonly gamePaddingHandler: GamePaddingHandler;
	protected pauseGameHandler: PauseGameHandler | undefined;
	protected lastValidGCoreStates: string[] = [];

	constructor(commandInjector: Injector, messageBroker: MessageBroker) {
		this.commandInjector = commandInjector;
		this.messageBroker = messageBroker;
		this.gamePaddingHandler = new GamePaddingHandler();
	}
	/**
	 * LoadingSequence:
	 * In Bolt we have a pre-made loading sequence you can use for your game that satisfies
	 * the requirements listed in the GDK here:
	 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/developer-guide/loading-sequence/
	 *
	 * Parts of this LoadingSequence is customisable. This is shown below where we customise the
	 * intro screen and loading screen layout via the `showLoadingScreenBehaviourArgs`
	 */
	public async init(): Promise<void> {
		this.subscribeToTopics();

		this.pauseGameHandler = new PauseGameHandler(this.messageBroker, this.commandInjector);

		buildCommandController(this.commandInjector, this.messageBroker);
		await initLoadingSequence(this.commandInjector, this.messageBroker, {
			bucketSizes,
			showLoadingScreenBehaviourArgs: {
				topic: LAYOUT_SET_LAYOUT,
				introLayoutUri: LayoutIntro,
			},
			initGameComms,
			scalingCallback: this.onResize.bind(this),
		})
			.execute(this.commandInjector, this.messageBroker)
			.then(() => {
				this.messageBroker.getTopic(REQUEST_HANDSHAKE).publishAsync();
			})
			.catch((err: unknown) => {
				console.error(err);
			});
	}

	private subscribeToTopics(): void {
		this.messageBroker.getTopic(LOADING_SEQUENCE_RENDERING_STARTED).subscribe(this.onRenderingStarted.bind(this), GameController.name);
		this.messageBroker.getTopic(LOADING_SEQUENCE_GAME_UI_INITIALISED).subscribe(this.onGameUIInitialised.bind(this), GameController.name);
		this.messageBroker.getTopic(GAME_LOADED).subscribe(this.onGameLoaded.bind(this), GameController.name);
		this.messageBroker.getTopic(LAYOUT_SET_LAYOUT).subscribe(this.onSetLayout.bind(this), GameController.name);
		this.messageBroker.getTopic(LAYOUT_SUBSTATE_CHANGED).subscribe(this.onLayoutSubstateChanged.bind(this), GameController.name);
		this.messageBroker.getTopic(SlotLayoutEvents.AUTO_PLAY_STOP).subscribe(this.onAutoPlayStop.bind(this), GameController.name);
	}

	/**
	 * GameBridge and GameUI event handlers:
	 * In Bolt we have GameBridgeHandlers and GameUIHandlers both will take care of most standard
	 * events.These are initialised in the `onGameUIInitialised` method at the bottom of the file.
	 * For GameUI handlers, depending on the game and layout type they may handle certain events
	 * differently. For that reason we're using specific handlers for slot games here.
	 */
	protected async onGameUIInitialised(): Promise<void> {
		new GameBridgeHandlers(this.commandInjector, this.messageBroker).addSubtitutions(this.getGameBridgeHandlerSubstitutions()).register();
		new SlotGameUIHandlers(this.commandInjector, this.messageBroker).addSubtitutions(this.getGameUIHandlerSubstitutions()).register();
	}

	protected getGameBridgeHandlerSubstitutions(): Map<string, GameBridgeEventHandler> {
		return new Map<string, GameBridgeEventHandler>([
			[GameBridgeEventNames.RESIZE_BLOCKER_ON, this.generateBasicGameBridgeEventHandler(GameBridgeEventNames.RESIZE_BLOCKER_ON)],
			[GameBridgeEventNames.RESIZE_BLOCKER_OFF, this.generateBasicGameBridgeEventHandler(GameBridgeEventNames.RESIZE_BLOCKER_OFF)],
			[GameBridgeEventNames.FOCUS_OFF, this.generateBasicGameBridgeEventHandler(GameBridgeEventNames.FOCUS_OFF)],
			[GameBridgeEventNames.FOCUS_ON, this.generateBasicGameBridgeEventHandler(GameBridgeEventNames.FOCUS_ON)],
			[GameBridgeEventNames.PLAY_INTERRUPTED, this.stopAutoPlayHandler.bind(this)],
		]);
	}

	protected getGameUIHandlerSubstitutions(): Map<string, GameUIEventHandler> {
		return new Map<string, GameUIEventHandler>([
			[SlotLayoutEvents.NEW_COIN_SIZE, this.newCoinSizeHandler.bind(this)],
			[SlotLayoutEvents.NEW_BET_LINE, this.newBetLineHandler.bind(this)],
			[SlotLayoutEvents.AUTO_PLAY_STOP, this.stopAutoPlayHandler.bind(this)],
		]);
	}

	protected async onRenderingStarted(): Promise<void> {
		this.initializeLayerStage();
	}

	private initializeLayerStage(): void {
		const pixiApplicationHandler = this.commandInjector.get(PixiApplicationHandler);
		console.assert(!pixiApplicationHandler.app?.stage.children.length);
		if (pixiApplicationHandler.app) {
			pixiApplicationHandler.app.stage.destroy();
			pixiApplicationHandler.app.stage = new Stage();
		}
	}

	protected async onGameLoaded(): Promise<void> {}

	protected async onSetLayout(message: IMessage): Promise<void> {
		const layoutName = message.data.layout;
		const layout = this.commandInjector.get(AssetManager).getAsset(layoutName).data;

		for (const layoutStateName in layout.states) {
			const paddingData = layout.states[layoutStateName].meta.bucket.padding;
			this.gamePaddingHandler.setGamePadding(layoutName, layoutStateName, paddingData);
		}

		this.gamePaddingHandler.onLayoutChanged(layoutName);
	}

	protected async onLayoutSubstateChanged(message: IMessage): Promise<void> {
		this.gamePaddingHandler.onSubstateChanged(message.data.substate);
	}

	protected async onAutoPlayStop(): Promise<void> {
		this.stopAutoPlayHandler();
	}

	protected generateBasicGameBridgeEventHandler(topic: string): GameBridgeEventHandler {
		return () => this.messageBroker.getTopic(topic).publish();
	}

	protected newCoinSizeHandler(injector: Injector, messageBroker: MessageBroker, coinSizeData: ICoinSizeData): void {
		SlotGameUIHandlers.newCoinSizeHandler(injector, messageBroker, coinSizeData);
		this.messageBroker.getTopic(SlotLayoutEvents.NEW_COIN_SIZE).publishAsync({
			topic: SlotLayoutEvents.NEW_COIN_SIZE,
			data: coinSizeData,
		});
	}

	protected newBetLineHandler(injector: Injector, messageBroker: MessageBroker, lines: number): void {
		SlotGameUIHandlers.newBetLineHandler(injector, messageBroker, lines);
		this.messageBroker.getTopic(SlotLayoutEvents.NEW_BET_LINE).publishAsync({
			topic: SlotLayoutEvents.NEW_BET_LINE,
			data: lines,
		});
	}

	protected stopAutoPlayHandler(): void {
		const store: WritableStore<GameStoreModel> = this.commandInjector.get(WritableStore);
		if (store.getState(StoreKeys.AUTOPLAY)) {
			store.setState(StoreKeys.AUTOPLAY, false, true);

			const gameUI = this.commandInjector.get(GameUI) as SlotGameUI;
			gameUI.layout.autoPlay.stop();
			requestAutoSpinTimeOut?.clear();
			if (!store.getState(StoreKeys.SPINNING)) {
				gameUI.layout.spin.spinComplete();
				window.gcore.getCommon().gameBridge.gameNotBusy();
			}
		}
	}

	protected onResize(data: IGCoreBucketPositionData): void {
		this.commandInjector.get(PixiApplicationHandler).resize(data);

		const store: WritableStore<GameStoreModel> = this.commandInjector.get(WritableStore);
		const orientation = window.gcore.display.bucket.getPosition().orientation;
		const previousIsPortrait = store.getState(StoreKeys.IS_PORTRAIT);
		const isPortrait = data.orientation === 'portrait';
		const currentValidGCoreStates = window.gcore.display.bucket.getPosition().states;
		const validStatesChanged = JSON.stringify(currentValidGCoreStates) !== JSON.stringify(this.lastValidGCoreStates);

		if (isPortrait !== previousIsPortrait) {
			store.setState(StoreKeys.IS_PORTRAIT, isPortrait, true);
			const orientationData: IOrientationChangeData = {
				orientation,
				isPortrait,
				isLandscape: !isPortrait,
			};
			this.messageBroker.getTopic(GameBridgeEventNames.ORIENTATION_CHANGE).publishAsync({
				data: orientationData,
			});
		}

		if (validStatesChanged) {
			this.lastValidGCoreStates = currentValidGCoreStates.slice();
			this.messageBroker.getTopic(LAYOUT_STATES_CHANGED).publishAsync({ data });
			this.gamePaddingHandler.onLayoutStatesChanged(data);
		}

		const isAutoPlay = !!store.getState(StoreKeys.AUTOPLAY);
		if (isAutoPlay) {
			this.stopAutoPlayHandler();
		}
	}
}
