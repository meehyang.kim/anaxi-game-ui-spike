/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/gcore/usage/
 *
 * Gcore is initialised in game-loader.js and is available as a property of window
 *
 * You access it by calling
 *
 * // as a property
 * window.gcore
 *
 * Game should only be created and started if gcore is available
 */
import { IGCore } from '@roxorgaming/types-gcore';
import { Game } from './Game';
((): void => {
	if ((window as { gcore: IGCore }).gcore) {
		const app = new Game();
		app.start();
	} else {
		throw new Error('Game failed to load because Gcore is unavailable!');
	}
})();
