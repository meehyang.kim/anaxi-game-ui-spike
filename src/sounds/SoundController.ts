/**
 * This class allows manual control over sounds. For example, stopping a base game sound
 * or more complex sound behaviour.
 */
import { IAudioPlayer, IAudio } from '@roxorgaming/bolt-audio-library';
import { SoundKey } from './SoundKey';

export class SoundController {
	private readonly audioPlayer: IAudioPlayer;

	constructor(audioPlayer: IAudioPlayer) {
		this.audioPlayer = audioPlayer;
	}

	public init(): void {
		// Subscribe to topics here
	}
	public getSound(key: SoundKey): IAudio | undefined {
		return this.audioPlayer.getSound(key);
	}
}
