/**
 * Typedef mapped to audio_data keys
 */

import audio_data from '../../resources/default/configs/audio_data.json';
export type SoundKey = keyof typeof audio_data;
