/**
 * Generic configuration file
 */

/**
 * Bucket sizes is specific to each game
 * though realistically most of the same
 * game type will likely have the exact
 * same value. Customise this for your
 * game if needed
 */
export const bucketSizes = {
	width: 320,
	height: 200,
	extendedWidth: 640,
	extendedHeight: 400,
	maxScale: 8,
};
