/**
 * All main layout urls.
 */
export const LayoutIntro = 'live-layout/intro-screen.layout.json';
export const LayoutRoot = 'live-layout/root.layout.json';
