/*
 * StatefulData is where we store any state that needs to be retained during 'stateful' play (e.g. a pick bonus game or free spin).
 * We usually store a snapshot of the last FBE response (as well as any data that might be useful such as bonus pick history) and
 * use that to generate the subsequent response. We can also intialise this class from the pre game menu with fake data so that
 * the handshake event handler can simulate a handshake mid way througha stateful game.
 */

import { AvailableBets, CustomFakeBackendConfig, GameSymbolType } from './fakeBackendSlotConfig';
import { GameReelInfo, GameReferenceInfo, GameSlotResultInfo, GameWageringInfo, NextAction, NextActionInfo } from '@roxorgaming/bolt-fake-backend';

//------------------------------------------------
export class StatefulData {
	//------------------------------------------------
	public GameReferenceInfo?: GameReferenceInfo;
	public GameWageringInfo?: GameWageringInfo;
	public GameSlotResultInfo?: GameSlotResultInfo<GameSymbolType>;
	public GameReelInfo?: GameReelInfo<GameSymbolType>;
	public NextActionInfo?: NextActionInfo;

	//------------------------------------------------
	constructor() {
		// Dummy data for a thereoretical handshake into a free spin round. Construct your own response data here.
		this.GameReferenceInfo = {
			gameReference: '9221b69b-30be-4f34-8ba9-eef19d6f06f9',
		};
		this.GameWageringInfo = {
			availableCoinSizes: CustomFakeBackendConfig.coinSizeInfo.coinSizes,
			defaultCoinSize: CustomFakeBackendConfig.coinSizeInfo.defaultCoinSize,
			currentCoinSize: CustomFakeBackendConfig.coinSizeInfo.defaultCoinSize,
			availableBets: AvailableBets,
			currentBets: AvailableBets,
		};
		this.GameSlotResultInfo = {
			grossWin: 0,
			gameWins: [],
			slotResult: [
				[5,7,8],
				[3,7,9],
				[9,7,1],
				[4,8,9],
				[5,7,6]
			],
			reelStopIndexes: [-1, -1, -1, -1, -1],
		};

		this.NextActionInfo = {
			nextAction: NextAction.FREE_SPIN,
		};
	}
}
