import { IInjector } from '@roxorgaming/bolt-injector';
import { Panel } from '@roxorgaming/bolt-fake-backend';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';

/* eslint-disable-next-line */
const helpPanelHtml = require('../html/helpPanel.html');

export class HelpPanel extends Panel {
	constructor(injector: IInjector, messageBroker: IMessageBroker) {
		super(injector, messageBroker, helpPanelHtml);
	}
}
