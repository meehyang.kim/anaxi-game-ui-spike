import { BaseForcerPanel, SlotGame } from '@roxorgaming/bolt-fake-backend';
import { GameStoreModel } from '../../../store';
import { StoreKeys } from '@roxorgaming/bolt-slots-shared';
import { WritableStore } from '@roxorgaming/bolt-store';

export class ForcerPanel extends BaseForcerPanel {
	public override calculateWinPreview(forcerInputValue: string): number {
		const { reels } = SlotGame.parseForcerString(forcerInputValue);
		const slotGame = this.injector.get(SlotGame);
		const forcer = { id: '', reels };
		const spin = slotGame.doReelSpin(forcer);
		const store: WritableStore<GameStoreModel> = this.injector.get(WritableStore);
		const coinSize = store.getState(StoreKeys.COIN_SIZE);
		const bets = store.getState(StoreKeys.CURRENT_BETS);
		const win = slotGame.findWins(spin, coinSize, bets);
		return win.grossWin;
	}
}
