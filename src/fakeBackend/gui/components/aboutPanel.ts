import { IInjector } from '@roxorgaming/bolt-injector';
import { Panel } from '@roxorgaming/bolt-fake-backend';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
import { PixiApplicationHandler } from '@roxorgaming/bolt-display';
import { RENDERER_TYPE } from 'pixi.js';

/* eslint-disable-next-line */
const packageJSON = require('../../../../package.json');
/* eslint-disable-next-line */
const aboutHTML = require('../html/aboutPanel.html');

export class AboutPanel extends Panel {
	constructor(injector: IInjector, messageBroker: IMessageBroker) {
		super(injector, messageBroker, aboutHTML);

		const modalBody = document.getElementById('about-modal-body') as HTMLDivElement;

		const pixiVer = packageJSON.dependencies['pixi.js'];
		const bucketData: any = window.gcore.display.bucket;
		const bucket = bucketData._bucket.width + 'x' + bucketData._bucket.height + ' scale-' + bucketData._scale;
		const pixiRenderer = injector.get(PixiApplicationHandler).app?.renderer;
		const renderInfo = pixiRenderer?.type === RENDERER_TYPE.WEBGL ? 'WebGL' : 'Canvas';

		modalBody.innerHTML = 'Version: ' + packageJSON.version + '<br>' + 'Pixi: ' + pixiVer + '<br>' + 'Bucket: ' + bucket + '<br>' + 'Renderer: ' + renderInfo;
	}
}
