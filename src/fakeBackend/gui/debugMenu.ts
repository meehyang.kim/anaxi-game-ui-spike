/* eslint-disable */
import { BaseDebugMenu, ForcerJSON } from '@roxorgaming/bolt-fake-backend';
import { IInjector } from '@roxorgaming/bolt-injector';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
import { AboutPanel, ForcerPanel, HelpPanel } from './components';
const forcers: ForcerJSON[] = require('./forcers.json');


export class DebugMenu extends BaseDebugMenu {
	private readonly aboutPanel: AboutPanel;
	private readonly helpPanel: HelpPanel;

	// Fake backend forcers. Customise / add your oen forcers here...
	private readonly menuConfig: any = {
		'No win?': () => this.addForcer('No win', '[reels:[[5,5,1], [6,6,2], [7,7,3], [8,8,4], [9,9,10]]]'),
		'Wins?': {
			'onClick': (choice: any, key: string) => this.addForcer(key, choice),
			'Single win': '[reels:[[9,7,8], [5,7,3], [8,7,9], [2,7,5], [9,6,8]]]',
			'Multi win': '[reels:[[9,7,8], [9,7,8], [9,7,8], [9,7,8], [9,7,8]]]',
			'Scatter win': '[reels:[[10,7,7], [5,4,3], [8,6,10], [7,2,5], [10,7,8]]]',
		},
		...this.defaultMenuConfig,
		'About': () => this.aboutPanel.toggle(),
		'Help': () => this.helpPanel.toggle(),
	};

	constructor(injector: IInjector, messageBroker: IMessageBroker) {
		super(injector, messageBroker);

		const forcerPanel = this.fakeBackend ?
			new ForcerPanel(injector, messageBroker, forcers) :
			undefined;

		this.configure({
			menuConfig: this.menuConfig,
			keyBindings: this.defaultKeyBindings,
			timeWarpSpeeds: [0, 0.5, 1, 4],
			forcerPanel
		});

		this.aboutPanel = new AboutPanel(this.injector, messageBroker);
		this.helpPanel = new HelpPanel(this.injector, messageBroker);
	}
}
