import { addSession, BasePreGameMenu, getSessionsFromLocalStorage, IFakeFastReplaySession, saveSessionsToLocalStorage } from '@roxorgaming/bolt-fake-backend';
import { IInjector } from '@roxorgaming/bolt-injector';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
/* eslint-disable-next-line */
const preGameMenuHTML = require('./html/preGameMenu.html');

export class PreGameMenu extends BasePreGameMenu {
	private readonly balanceInput: HTMLInputElement;
	private readonly throttlingInput: HTMLInputElement;
	private readonly errorSelect: HTMLSelectElement;
	private readonly startButton: HTMLButtonElement;

	private readonly fakeReplaySelect: HTMLSelectElement;
	private readonly newFakeReplaySessionInput: HTMLInputElement;
	private readonly newFakeReplaySessionButton: HTMLButtonElement;

	constructor(injector: IInjector, messageBroker: IMessageBroker) {
		super(injector, messageBroker, preGameMenuHTML);
		this.balanceInput = document.getElementById('balance-input') as HTMLInputElement;
		this.throttlingInput = document.getElementById('throttling-input') as HTMLInputElement;
		this.errorSelect = document.getElementById('error-select') as HTMLSelectElement;
		this.startButton = document.getElementById('start-game-button') as HTMLButtonElement;

		this.newFakeReplaySessionInput = document.getElementById('new-session-input') as HTMLInputElement;
		this.fakeReplaySelect = document.getElementById('fake-replay-select') as HTMLSelectElement;
		this.newFakeReplaySessionButton = document.getElementById('add-new-session-button') as HTMLButtonElement;
		this.fakeReplaySelect.addEventListener('change', this.copySelectedFakeReplaySessionToClipboard.bind(this));

		this.createFakeReplaySelectOptions();
	}

	public override async show(): Promise<void> {
		this.modal.show();

		return new Promise<void>((resolve) => {
			this.balanceInput.placeholder = this.fakeBackend.getBalance().toFixed(2);

			const onStartClicked = (): void => {
				const balance = parseInt(this.balanceInput.value);
				const throttling = parseInt(this.throttlingInput.value);
				const error = parseInt(this.errorSelect.value);
				const fakeReplaySession = this.fakeReplaySelect.value;

				if (!isNaN(balance)) {
					this.fakeBackend.setBalance(balance);
				}

				if (!isNaN(throttling)) {
					this.fakeBackend.setThrottling(throttling * 1000);
				}

				switch (error) {
					case 1:
						this.fakeBackend.forceConnectionError();
						break;
					case 2:
						this.fakeBackend.forceRuntimeError();
						break;
					case 3:
						this.fakeBackend.forceMalformedRequestError();
						break;
				}

				if (fakeReplaySession !== 'none') {
					this.fakeBackend.applySelectedFakeReplaySession(fakeReplaySession);
				}

				resolve();
			};

			this.startButton.addEventListener('click', () => onStartClicked());
			this.newFakeReplaySessionButton.addEventListener('click', () => this.onAddSessionClicked());
		});
	}

	private createSessionDropdownItem(session: IFakeFastReplaySession): HTMLOptionElement {
		const lastResponse = session.responses[session.responses.length - 1];
		const unfinished = lastResponse.NextActionInfo?.nextAction !== 'SPIN';
		const endBalance = lastResponse.PlayerBalanceInfo?.balance ?? 0;
		const wager = session.responses[0].PlayerBalanceInfo?.wager ?? 0;
		const totalWin = endBalance - session.startingBalance + wager;
		const winString = unfinished ? 'UNFINISHED‼️' : `win: ${totalWin.toFixed(2)}`;
		const timeString = new Date(session.timestamp).toLocaleString().split(', ').join('-');
		const truncatedId = session.transactionId.split('-').pop();

		return new Option(`${timeString} / ${winString} / id: ...-${truncatedId}`, session.transactionId);
	}

	private createFakeReplaySelectOptions(): void {
		const sessions = getSessionsFromLocalStorage();
		if (!sessions.length) return;

		while (this.fakeReplaySelect.options.length > 1 && this.fakeReplaySelect.lastChild) {
			this.fakeReplaySelect.removeChild(this.fakeReplaySelect.lastChild);
		}

		sessions.forEach((session) => {
			this.fakeReplaySelect.add(this.createSessionDropdownItem(session));
		});
	}

	private copySelectedFakeReplaySessionToClipboard(e: any): void {
		const sessions = getSessionsFromLocalStorage();
		if (!sessions.length) return;

		const selectedSessionId = e.target.value;
		const session = sessions.find((s) => s.transactionId === selectedSessionId);
		session && navigator.clipboard.writeText(JSON.stringify(session));
	}

	private onAddSessionClicked(): void {
		try {
			const sessions = getSessionsFromLocalStorage();
			const inputValue = JSON.parse(this.newFakeReplaySessionInput.value) as IFakeFastReplaySession;
			if (inputValue.transactionId) {
				addSession(sessions, inputValue);
				saveSessionsToLocalStorage(sessions);
				this.newFakeReplaySessionInput.value = '';
				this.createFakeReplaySelectOptions();
			}
		} catch (e) {
			window.alert('Pasted session not in correct format');
		}
	}
}
