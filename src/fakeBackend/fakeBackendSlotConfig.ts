/*
 * Configure the fake backend.
 * Populate with your own game's config so as to match the real backend behaviour as close as possible.
 */

import { FakeBackendConfig, Paytable, ReelPosition, ReelStrip, CoinSizeInfo, WinType } from '@roxorgaming/bolt-fake-backend';

export type GameSymbolType = number;

export enum ReelStripType {
	BASE = 'BASE',
}

// Coin sizes
const CoinSizeInfo: CoinSizeInfo = {
	coinSizes: [0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 4, 6, 8, 10, 20],
	defaultCoinSize: 0.05,
};

// Active winlines
export const AvailableBets = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

// For each winline we define the symbol index per reel
const CustomWinlines = [
	[1,1,1,1,1],
	[0,0,0,0,0],
	[2,2,2,2,2],
	[0,1,2,1,0],
	[2,1,0,1,2],
	[1,2,1,2,1],
	[1,0,1,0,1],
	[2,1,2,1,2],
	[0,1,0,1,0],
	[0,0,1,2,2],
	[2,2,1,0,0],
	[0,1,1,1,2],
	[2,1,1,1,0],
	[1,1,2,2,2],
	[1,2,2,1,1]
];

// Paytable config. Each row configures the base win amount for 2, 3, 4, and 5 symbol matches
// going down from symbol 0 to symbol n. A '0' configures no win
const CustomPaytable: Paytable = [
	[0, 50, 200, 1000],
	[0, 50, 200, 1000],
	[0, 45, 175, 800],
	[0, 40, 150, 600],
	[0, 20, 100, 500],
	[0, 15, 80, 300],
	[0, 10, 40, 200],
	[0, 10, 30, 150],
	[0, 5, 25, 120],
	[0, 5, 20, 100],
	[0, 5, 20, 100],
];

const CustomSymbolNames = ['WILD', 'PIC1', 'PIC2', 'PIC3', 'PIC4', 'A', 'K', 'Q', 'J', '10', 'BONUS'];

// Base game reel strips
const BaseGameReelStrip: ReelStrip<GameSymbolType>[] = [
	[2,6,9,2,6,10,5,8,9,1,6,0,2,8,9,4,6,0,2,7,6,3,9,10,5,7,8,1,8,10,3,8,8,5,6,10,1,9,9,5,7,10,3,8,9,4,7,0,1,9,9,4,9,4,7,6,5,6,4,9,7,1,9,2,9,8,5,7,3,6,9,6,8,6,8,6,9],
	[1,8,8,2,6,0,5,7,6,3,7,0,5,9,8,4,7,0,2,8,7,4,6,0,3,7,9,4,7,0,2,8,8,2,9,3,7,9,4,8,5,7,7,3,8,2,6,6,4,9,1,8,7,4,6,3,8,8,4,8,3,9,7,5,9,3,9,6,5,7,4,9,9,5,3,3,5],
	[3,6,6,5,8,10,3,7,9,5,6,10,5,8,9,1,8,10,4,9,7,1,7,10,3,8,6,1,9,10,5,8,7,4,9,10,3,7,7,1,9,0,4,7,7,2,6,0,5,7,8,2,9,0,2,8,8,5,8,4,9,8,1,8,5,8,7,5,9,4,6,6,3,7,1,8,6,5,9],
	[4,8,9,3,7,0,2,9,7,5,9,0,3,8,7,4,7,0,2,7,6,3,7,0,4,8,7,5,6,0,4,6,8,3,7,5,6,8,2,9,5,6,9,4,9,4,7,7,4,8,3,9,6,4,9,1,6,6,3,7,1,8,8,5,7,2,9,8,5,5,2,3],
	[5,7,6,5,9,0,4,7,8,5,6,10,2,9,8,2,6,10,2,6,7,5,6,0,4,6,7,4,7,10,1,9,9,3,6,0,1,9,9,1,8,10,3,7,9,5,8,10,1,6,9,3,7,2,9,6,4,6,1,9,8,5,8,3,8,8,4,9,2,8,8,9]
];

// Initial reel indexes when we handshake into the base game.
const CustomInitialReelIndexes: number[] = [57,39,10,37,42];

// Initial reels when we handshake into the base game.
const CustomInitialReelStrip: ReelPosition<GameSymbolType> = [
	[6,4,9],
	[8,5,7],
	[6,10,5],
	[8,2,9],
	[3,7,9]
];

export const CustomFakeBackendConfig: FakeBackendConfig<GameSymbolType> = {
	winType: WinType.LINE,
	numSymbolsPerReel: 3,
	scatterSymbol: 10,
	wildSymbol: 0,
	isStateful: true,
	coinSizeInfo: CoinSizeInfo,
	winlines: CustomWinlines,
	symbolNames: CustomSymbolNames,
	paytable: CustomPaytable,
	initialReelPosition: CustomInitialReelStrip,
	initialReelIndexes: CustomInitialReelIndexes,
	defaultReelStripID: ReelStripType.BASE,
	reelStrips: {
		[ReelStripType.BASE]: BaseGameReelStrip,
	},
};
