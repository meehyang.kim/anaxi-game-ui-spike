import { GameSymbolType, CustomFakeBackendConfig } from './fakeBackendSlotConfig';
import { CustomFakeBackend } from './customFakeBackend';
import { FakeSlotBackend } from '@roxorgaming/bolt-fake-backend';
import { Injector } from '@roxorgaming/bolt-injector';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
import { initFakeBackend as initFakeBackendBolt } from '@roxorgaming/bolt-fake-backend';
import { DebugMenu, PreGameMenu } from './gui';
import { HandshakeEventHandler, SpinEventHandler, CustomSlotGame } from './slotGame';
import { StatefulData } from './statefulData';

export async function initFakeBackend(injector: Injector, messageBroker: IMessageBroker): Promise<void> {
	return initFakeBackendBolt(
		injector,
		messageBroker,
		fakeBackendBuilder,
		() => new PreGameMenu(injector, messageBroker),
		() => new DebugMenu(injector, messageBroker)
	);
}

function fakeBackendBuilder(injector: Injector, messageBroker: IMessageBroker): void {
	new CustomSlotGame<GameSymbolType>(injector, CustomFakeBackendConfig);

	const fakeBackend = new CustomFakeBackend(injector, messageBroker);
	const gamesBackend = new FakeSlotBackend<GameSymbolType, StatefulData>(injector, fakeBackend, CustomFakeBackendConfig);

	gamesBackend.attachEventHandler('Handshake', new HandshakeEventHandler(injector));
	gamesBackend.attachEventHandler('Spin', new SpinEventHandler(injector));
	// add your game's event handlers here.
}
