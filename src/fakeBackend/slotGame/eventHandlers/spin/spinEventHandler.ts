import {
	BaseGamesBackend,
	buildGameReferenceInfo,
	buildGameWageringInfo,
	buildPlayerBalanceInfo,
	calcWager,
	FakeBackend,
	FakeSlotBackend,
	IGamesEventHandler,
	logResponseToLocalStorage,
	NextAction,
	parseSpinPayload,
	ProgressivesInfo,
	ResponseError,
	SlotGame,
	SpinPayload,
	SpinResponse,
} from '@roxorgaming/bolt-fake-backend';
import { CustomFakeBackendConfig, GameSymbolType, ReelStripType } from '../../../fakeBackendSlotConfig';
import { Injector } from '@roxorgaming/bolt-injector';
import { CommunicationConstantsSCJson, ResponseSCJson } from '@roxorgaming/bolt-slots-communication';
import { CustomSlotGame } from '../../customSlotGame';
import { StatefulData } from '../../../statefulData';

type CustomSpinResponse = SpinResponse<GameSymbolType> & {
	ProgressivesInfo: ProgressivesInfo;
};

export class SpinEventHandler implements IGamesEventHandler<GameSymbolType> {
	private readonly injector: Injector;
	private readonly fakeBackend: FakeBackend;
	private readonly gamesBackend: FakeSlotBackend<GameSymbolType, StatefulData>;
	private readonly slotGame: CustomSlotGame<GameSymbolType>;

	constructor(injector: Injector) {
		this.injector = injector;
		this.fakeBackend = this.injector.get(FakeBackend);
		this.gamesBackend = this.injector.get(BaseGamesBackend) as FakeSlotBackend<GameSymbolType, StatefulData>;
		this.slotGame = this.injector.get(SlotGame) as CustomSlotGame<GameSymbolType>;
	}

	public handler(payload: SpinPayload): Promise<SpinResponse<GameSymbolType> | ResponseError> {
		const { forcer, numActiveWinlines, coinSize } = parseSpinPayload<GameSymbolType>(this.slotGame, payload);
		const { totalWager, preWagerBalance, postWagerBalance } = calcWager(this.injector, coinSize, numActiveWinlines);
		const currentReelMap = forcer.slotId ? forcer.slotId : ReelStripType.BASE;

		if (postWagerBalance < 0) {
			return Promise.resolve({ ErrorInfo: { type: CommunicationConstantsSCJson.InsufficientFunds } });
		}

		this.fakeBackend.setBalance(postWagerBalance);

		const reelPosition = this.slotGame.doReelSpin(forcer);
		const spinResults = this.slotGame.findWins(reelPosition, coinSize, numActiveWinlines);
		const nextAction = NextAction.SPIN;

		const response = {
			GameReferenceInfo: buildGameReferenceInfo(),
			GameReelInfo: { currentReelMap },
			PlayerBalanceInfo: buildPlayerBalanceInfo(this.injector, totalWager, preWagerBalance, postWagerBalance, spinResults.grossWin),
			GameWageringInfo: buildGameWageringInfo(CustomFakeBackendConfig, coinSize, numActiveWinlines),
			GameSlotResultInfo: spinResults,
			ProgressivesInfo: this.fakeBackend.getProgressives(),
			NextActionInfo: {
				nextAction,
			},
		} as CustomSpinResponse;

		if (nextAction !== NextAction.SPIN) {
			this.gamesBackend.setStatefulData(response);
		}

		logResponseToLocalStorage(response.GameReferenceInfo.gameReference, response.PlayerBalanceInfo?.preWagerBalance ?? 1000, response as ResponseSCJson);

		return Promise.resolve(response);
	}
}
