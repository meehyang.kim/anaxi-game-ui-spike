import {
	IGamesEventHandler,
	FakeSlotBackend,
	GameResponse,
	BaseGamesBackend,
	buildSimpleGameWageringInfo,
	buildSimplePlayerBalanceInfo,
	NextAction,
	GameReelInfo,
	GameReferenceInfo,
} from '@roxorgaming/bolt-fake-backend';
import { AvailableBets, CustomFakeBackendConfig, GameSymbolType } from '../../../fakeBackendSlotConfig';
import { Injector } from '@roxorgaming/bolt-injector';
import { StatefulData } from '../../../statefulData';

type CustomHandshakeResponse = GameResponse<GameSymbolType> & {
	GameReelInfo?: GameReelInfo<GameSymbolType>;
	GameReferenceInfo?: GameReferenceInfo;
};

export class HandshakeEventHandler implements IGamesEventHandler<GameSymbolType> {
	private readonly injector: Injector;
	private readonly gamesBackend: FakeSlotBackend<GameSymbolType, StatefulData>;

	constructor(injector: Injector) {
		this.injector = injector;
		this.gamesBackend = this.injector.get(BaseGamesBackend) as FakeSlotBackend<GameSymbolType, StatefulData>;
	}

	public handler(): Promise<CustomHandshakeResponse> {
		let response: CustomHandshakeResponse = {
			PlayerBalanceInfo: buildSimplePlayerBalanceInfo(this.injector),
			GameReelInfo: {
				reelMapDatas: [
					{
						name: 'BASE',
						reelMap: CustomFakeBackendConfig.reelStrips['BASE'],
					},
				],
				reelStopIndexes: CustomFakeBackendConfig.initialReelIndexes,
				reelValues: CustomFakeBackendConfig.initialReelPosition,
				currentReelMap: 'BASE',
			},
			GameWageringInfo: buildSimpleGameWageringInfo(CustomFakeBackendConfig, AvailableBets.length),
			NextActionInfo: {
				nextAction: NextAction.SPIN,
			},
		};

		const statefulData = this.gamesBackend.getStatefulData();

		if (statefulData) {
			response = {
				...response,
				...statefulData,
			};
		}

		if (response.NextActionInfo?.nextAction !== NextAction.SPIN) {
			this.gamesBackend.setStatefulData(response);
		}

		return Promise.resolve(response);
	}
}
