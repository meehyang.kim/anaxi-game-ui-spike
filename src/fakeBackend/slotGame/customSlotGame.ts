import { FakeBackendConfig, SlotGame } from '@roxorgaming/bolt-fake-backend';
import { Injector } from '@roxorgaming/bolt-injector';

export class CustomSlotGame<S> extends SlotGame<S> {
	constructor(injector: Injector, slotConfig: FakeBackendConfig<S>) {
		super(injector, slotConfig);
	}
}
