/**
 * The Intro Screen View Component
 */
import { AssetManager } from '@roxorgaming/bolt-assets';
import { AnimationManager, BoltTween, ViewComponent, ViewConstructorProps } from '@roxorgaming/bolt-display';
import { LoadingSequenceTopics, StoreKeys } from '@roxorgaming/bolt-loading-sequence';
import { ReadableStore } from '@roxorgaming/bolt-store';
import { Container, Sprite, Text } from 'pixi.js';
import { INTRO_SCREEN_SOUND_BUTTON_CLICKED, LOADING_SET_PROGRESS, REQUEST_HANDSHAKE_SUCCESS } from '@roxorgaming/bolt-slots-shared';
import { IStateHandler, StateHandler, StateHandlerEvents } from '@roxorgaming/bolt-utils';
import { BoltLocaliser } from '@roxorgaming/bolt-localisation';

const targetProgressBeforeLoadSound = 0.6;
const targetProgressAfterLoadSound = 0.95;

export class IntroScreenComponent extends ViewComponent {
	private animationManager: AnimationManager;
	private localiser: BoltLocaliser;
	protected readonly progress: IStateHandler<number>;
	private progressTween: BoltTween | null;
	private soundFileCount: number;
	private mainFileCount: number;
	private totalFileCount: number;
	private loadedFileCount: number;
	private startTime: number;
	private targetProgress: number;

	constructor(props: ViewConstructorProps<void>) {
		super(props);

		this.animationManager = this.injector.get(AnimationManager);

		this.progress = new StateHandler<number>(0);
		this.progress.events.on(StateHandlerEvents.changed, this.updateProgress, this);

		this.localiser = this.injector.get(BoltLocaliser);
		const assetManager = this.injector.get(AssetManager);
		this.soundFileCount = assetManager.getFileCount('sounds');
		this.mainFileCount = assetManager.getFileCountForGroup('main');
		this.progressTween = null;
		this.totalFileCount = this.soundFileCount + this.mainFileCount;
		this.loadedFileCount = 0;
		this.startTime = 0;
		this.targetProgress = 0;
	}

	public build(): void {
		const store = this.injector.get(ReadableStore);
		const canLoadAudio = !!store.getState(StoreKeys.CAN_LOAD_AUDIO);

		this.initLoadingBar();
		this.setLoadingBarVisible(!canLoadAudio);

		if (canLoadAudio) {
			this.soundMessageText.text = this.localiser.getLocalisedText({ key: 'PLAY_WITH_SOUND?' });
			this.initButtons();
		} else {
			this.onSoundButtonClicked(false);
		}

		this.messageBroker.getTopic(REQUEST_HANDSHAKE_SUCCESS).subscribe(this.onHandshakeSuccess, IntroScreenComponent.name);
		this.messageBroker.getTopic(LoadingSequenceTopics.GAME_LOADED_WITH_SOUND_PREF).subscribe(this.onGameLoaded, IntroScreenComponent.name);

		this.startTime = Date.now();
		this.targetProgress = targetProgressBeforeLoadSound;
	}

	private initLoadingBar(): void {
		this.loadingBar.scale.x = 0;
		this.messageBroker.getTopic(LOADING_SET_PROGRESS).subscribe(this.setProgress, IntroScreenComponent.name);
	}

	private updateProgress = (): void => {
		this.loadingBar.scale.x = this.progress.state > 1 ? 1 : this.progress.state;
	};

	private setProgress = async (): Promise<void> => {
		this.loadedFileCount++;
		this.updateTween();
	};

	private updateTween(duration?: number): Promise<void> {
		return new Promise((resolve) => {
			if (this.progressTween) {
				this.progressTween.stop();
				this.animationManager.removeAnimation(this.progressTween);
			}
			this.progressTween = this.animationManager.tween(this.progress, {
				tweens: [
					{
						target: { state: this.targetProgress },
						duration: duration || this.getEstimatedRemainingTime(),
					},
				],
				onComplete: {
					handler: () => {
						resolve();
					},
				},
			});
		});
	}

	private onGameLoaded = async (): Promise<void> => {
		this.targetProgress = 1;
		await this.updateTween(1000);
	};

	private getEstimatedRemainingTime(): number {
		const now = Date.now();
		const elapsed = now - this.startTime;
		const avgTimePerAsset = elapsed / this.loadedFileCount;
		const remainingTime = (this.totalFileCount - this.loadedFileCount) * avgTimePerAsset;
		return remainingTime || 60000;
	}

	private initButtons(): void {
		const yesButton = this.yesButton;
		yesButton.interactive = true;
		yesButton.buttonMode = true;
		yesButton.on('click', this.onSoundButtonClicked.bind(this, true));
		yesButton.on('tap', this.onSoundButtonClicked.bind(this, true));

		const noButton = this.noButton;
		noButton.interactive = true;
		noButton.buttonMode = true;
		noButton.on('click', this.onSoundButtonClicked.bind(this, false));
		noButton.on('tap', this.onSoundButtonClicked.bind(this, false));

		this.yesButtonText.text = this.localiser.getLocalisedText({ key: 'YES' });
		this.noButtonText.text = this.localiser.getLocalisedText({ key: 'NO' });
	}

	private deInitButtons(): void {
		this.yesButton.removeAllListeners();
		this.noButton.removeAllListeners();
	}

	private onSoundButtonClicked(soundOn: boolean): void {
		this.messageBroker.getTopic(INTRO_SCREEN_SOUND_BUTTON_CLICKED).publishAsync({
			topic: INTRO_SCREEN_SOUND_BUTTON_CLICKED,
			data: {
				soundOn,
			},
		});
		this.setLoadingBarVisible(true);
		this.deInitButtons();

		this.targetProgress = targetProgressAfterLoadSound;
		this.updateTween();
	}

	private onHandshakeSuccess = async (): Promise<void> => {
		this.messageBroker.getTopic(REQUEST_HANDSHAKE_SUCCESS).unsubscribe(this.onHandshakeSuccess, IntroScreenComponent.name);
		this.messageBroker.getTopic(LOADING_SET_PROGRESS).unsubscribe(this.setProgress, IntroScreenComponent.name);
		this.messageBroker.getTopic(LoadingSequenceTopics.GAME_LOADED_WITH_SOUND_PREF).unsubscribe(this.onGameLoaded, IntroScreenComponent.name);
	};

	private setLoadingBarVisible(visible: boolean): void {
		this.loadingBarPanel.visible = visible;
		this.soundOptionPanel.visible = !visible;
	}

	private get loadingBarPanel(): Container {
		return this.container.getChildByName('loading-bar-panel');
	}

	private get soundOptionPanel(): Container {
		return this.container.getChildByName('sound-option-panel');
	}

	private get loadingBar(): Sprite {
		return this.loadingBarPanel.getChildByName('loading-bar');
	}

	private get yesButton(): Sprite {
		return this.soundOptionPanel.getChildByName('btn-yes');
	}

	private get yesButtonText(): Text {
		return this.soundOptionPanel.getChildByName('btn-yes-text');
	}

	private get noButton(): Sprite {
		return this.soundOptionPanel.getChildByName('btn-no');
	}

	private get noButtonText(): Text {
		return this.soundOptionPanel.getChildByName('btn-no-text');
	}

	private get soundMessageText(): Text {
		return this.soundOptionPanel.getChildByName('sound-message-text');
	}
}