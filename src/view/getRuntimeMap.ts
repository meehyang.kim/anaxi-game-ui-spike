/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/view-components/
 *
 * This function provides the mapping between a layout and a ViewComponent class
 */
import { RunTimeConstructionData } from '@roxorgaming/bolt-display';
import { ReelsViewComponent } from './ReelsViewComponent';
import { WinlinesViewComponent } from './WinlinesViewComponent';
import { IntroScreenComponent } from './IntroScreenComponent';
import { OrientationStateComponent } from './OrientationStateComponent';

export function getRuntimeMap(): RunTimeConstructionData {
	const runtimeMap: RunTimeConstructionData = new RunTimeConstructionData();
	runtimeMap.add('base-game', OrientationStateComponent);
	runtimeMap.add('reels', ReelsViewComponent);
	runtimeMap.add('winlines', WinlinesViewComponent);
	runtimeMap.add('intro-screen', IntroScreenComponent);

	return runtimeMap;
}
