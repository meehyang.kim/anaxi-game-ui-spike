/**
 * Root View Component, manages state for the entire root view hierarchy.
 */

import { UpdateChildrenOption, ViewComponent, ViewConstructorProps } from '@roxorgaming/bolt-display';
import { LAYOUT_STATES_CHANGED } from '@roxorgaming/bolt-slots-shared';

export class OrientationStateComponent extends ViewComponent {
	constructor(props: ViewConstructorProps<void>) {
		super(props);
	}

	public build(): void {
		this.messageBroker.getTopic(LAYOUT_STATES_CHANGED).subscribe(this.onLayoutStatesChangedEvent.bind(this), OrientationStateComponent.name);

		this.stateManager.updateSettings({
			treeDepth: Infinity,
			updateChildren: UpdateChildrenOption.ALL,
		});

		this.updateState();
	}

	protected async onLayoutStatesChangedEvent(): Promise<void> {
		this.updateState();
	}

	protected updateState(): void {
		this.stateManager.updateState(this.container, this.definition);
	}
}
