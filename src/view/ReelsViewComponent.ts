/**
 * Controls all reels functionality including iterating wins.
 */

import { AnimationManager, IPoint, ViewComponent, ViewConstructorProps } from '@roxorgaming/bolt-display';
import {
	IReelControllerSpinningConfig,
	IReelsModel,
	ISpineConfig,
	ISpriteGraph,
	ISymbolOrderConfig,
	ISymbolPosition,
	ReelControllerEvents,
	ReelControllerSpinning,
	ReelSet,
	ReelSetParams,
	ReelSymbol,
	SymbolAnimation,
} from '@roxorgaming/bolt-slots-reels';
import { AssetManager } from '@roxorgaming/bolt-assets';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { BoltTime, IBoltTimer, IStateHandler, StateHandler, StateHandlerEvents, Ticker } from '@roxorgaming/bolt-utils';
import { GameUITopics, SlotLayoutEvents } from '@roxorgaming/bolt-loading-sequence';
import { ReadableStore } from '@roxorgaming/bolt-store';
import { IReelStrips, IWin, WinType } from '@roxorgaming/bolt-slots-communication';
import { GameSettings } from '../settings';
import { getEaseFunctionForConfig } from '@roxorgaming/bolt-easing';
import { createSlotGrid, setLineWinText, setScatterWinText, setSpinReady } from '../utils';
import EventEmitter from 'eventemitter3';
import { Graphics } from 'pixi.js';
import {
	SET_REEL_MAPS,
	SET_INIT_REELS,
	SHOW_WIN_CYCLE,
	SHOW_WINLINE,
	SHOW_WINLINES,
	SHOW_WINS_ALL,
	SHOW_WIN_ITERATION,
	SPIN_REELS,
	StoreKeys,
} from '@roxorgaming/bolt-slots-shared';

interface IEventTypes {
	LandComplete: (symbol: ReelSymbol) => void;
	AllLandComplete: void;
}

export class ReelsViewComponent extends ViewComponent {
	protected readonly events: EventEmitter<IEventTypes>;
	protected readonly reelSet: ReelSet;
	protected readonly reelControllers: ReelControllerSpinning[];
	protected readonly assetManager: AssetManager;
	protected readonly animationManager: AnimationManager;

	protected reelStripsConfigurations: Map<string, IReelStrips> | undefined;
	protected readonly currentReelStrip: IStateHandler<string | undefined>;

	protected readonly settings: GameSettings;
	protected readonly bucketScale: number;
	protected readonly baseSymbolWidth: number;
	protected readonly baseSymbolHeight: number;

	protected currentSpinConfig: IReelsModel | undefined;
	protected skipWinTimer: IBoltTimer | undefined | null;
	protected skipWinFunction: (() => void) | undefined;
	protected shouldSkipWinHighlights: boolean;

	protected readonly ticker: Ticker;
	protected readonly time: BoltTime;

	constructor(props: ViewConstructorProps) {
		super(props);

		const store = this.injector.get(ReadableStore);
		this.ticker = this.injector.get(Ticker);
		this.time = this.injector.get(BoltTime);
		this.settings = store.getState(StoreKeys.SETTINGS) as GameSettings;
		this.events = new EventEmitter<IEventTypes>();

		this.animationManager = this.injector.get<AnimationManager>(AnimationManager);
		this.assetManager = this.injector.get<AssetManager>(AssetManager);
		this.bucketScale = window.gcore.display.bucket.getScale();
		this.baseSymbolWidth = this.settings.reels.baseSymbolWidth * this.bucketScale;
		this.baseSymbolHeight = this.settings.reels.baseSymbolHeight * this.bucketScale;
		this.shouldSkipWinHighlights = false;

		this.reelStripsConfigurations = undefined;
		this.currentReelStrip = new StateHandler<string | undefined>(undefined);
		this.currentReelStrip.events.on(StateHandlerEvents.changed, this.changeReelStrip, this);

		const reelSetParams: ReelSetParams = this.getReelSetParams();
		this.reelSet = new ReelSet(reelSetParams);
		this.reelControllers = [];

		for (let reelIndex = 0; reelIndex < reelSetParams.grid.length; ++reelIndex) {
			const controller: ReelControllerSpinning = new ReelControllerSpinning(this.getControllerConfig(reelIndex));
			controller.events.on(ReelControllerEvents.symbolStoppingEvent, this.onSymbolLanded, this);
			controller.events.on(ReelControllerEvents.reelStoppingEvent, this.onReelLanded, this);
			this.reelControllers.push(controller);

			controller.boundarySymbolsTop.state = this.settings.reels.boundarySymbolsTop;
			controller.boundarySymbolsBottom.state = this.settings.reels.boundarySymbolsBottom;
			controller.spinSpeed.state = this.settings.reels.spinSpeed * this.bucketScale;
			controller.blurWhenSpinning.state = true;

			controller.startingBounce.delay = this.settings.reels.startingBounce.delay;
			controller.startingBounce.outDuration = this.settings.reels.startingBounce.outDuration;
			controller.startingBounce.magnitude = this.settings.reels.startingBounce.magnitude * this.bucketScale;
			controller.startingBounce.outEasing = getEaseFunctionForConfig(this.settings.reels.startingBounce.outEasing);

			controller.stoppingBounce.delay = this.settings.reels.stoppingBounce.delay;
			controller.stoppingBounce.outDuration = this.settings.reels.stoppingBounce.outDuration;
			controller.stoppingBounce.inDuration = this.settings.reels.stoppingBounce.inDuration;
			controller.stoppingBounce.magnitude = this.settings.reels.stoppingBounce.magnitude * this.bucketScale;
			controller.stoppingBounce.outEasing = getEaseFunctionForConfig(this.settings.reels.stoppingBounce.outEasing);
			controller.stoppingBounce.inEasing = getEaseFunctionForConfig(this.settings.reels.stoppingBounce.inEasing);
		}

		this.ticker.add(this.update.bind(this));

		// Create a mask to hide boundary symbols.
		const mask = this.createReelMask();
		this.container.addChild(mask);
		this.container.mask = mask;
	}

	public build(): void {
		console.log('ReelsViewComponent:build');
		this.messageBroker.getTopic(SET_INIT_REELS).subscribe(this.setInitialReels.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SET_REEL_MAPS).subscribe(this.setReelMaps.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SPIN_REELS).subscribe(this.spinReels.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(GameUITopics.REQUEST_SPIN).subscribe(this.skipWin.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SlotLayoutEvents.NEW_COIN_SIZE).subscribe(this.skipWin.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SlotLayoutEvents.NEW_BET_LINE).subscribe(this.skipWin.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SHOW_WINS_ALL).subscribe(this.showWinsAll.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SHOW_WIN_CYCLE).subscribe(this.showWinCycle.bind(this), ReelsViewComponent.name);
		this.messageBroker.getTopic(SHOW_WIN_ITERATION).subscribe(this.showWinIteration.bind(this), ReelsViewComponent.name);
	}

	protected async setInitialReels(message: IMessage): Promise<void> {
		if (message.data != null) {
			this.reelSet.setReelsToResult({ reelResults: message.data });
		}
	}

	protected async spinReels(message: IMessage): Promise<void> {
		const spinConfig: IReelsModel = message.data;
		this.currentSpinConfig = spinConfig;
		this.shouldSkipWinHighlights = false;

		console.log('ReelsViewComponent: spinReels with data:', spinConfig);

		const activeControllers: ReelControllerSpinning[] = [...this.reelControllers];
		let extraRegulationDuration = 0;

		if (window.gcore.regulations.minTimeBetweenWagers > 0) {
			const maxExtraReelDuration: number = this.settings.reels.spinDurationExtraPerReel * (activeControllers.length - 1);
			const totalBounceDuration: number =
				this.settings.reels.startingBounce.delay +
				this.settings.reels.startingBounce.outDuration +
				this.settings.reels.stoppingBounce.delay +
				this.settings.reels.stoppingBounce.inDuration +
				this.settings.reels.stoppingBounce.outDuration;
			const minResultingDuration: number =
				(this.baseSymbolHeight * (this.settings.reels.numberOfRows - 1 + this.settings.reels.boundarySymbolsTop + this.settings.reels.boundarySymbolsBottom)) /
				(this.settings.reels.spinSpeed * this.bucketScale);
			const minPossibleDuration = this.settings.reels.spinDuration + maxExtraReelDuration + totalBounceDuration + minResultingDuration;

			extraRegulationDuration = Math.max(0.0, window.gcore.regulations.minTimeBetweenWagers - minPossibleDuration);

			console.log('minTimeBetweenWagers Regulation - minTimeBetweenWagers:', window.gcore.regulations.minTimeBetweenWagers);
			console.log('minTimeBetweenWagers Regulation - Previous Min Possible Spin Duration:', minPossibleDuration);
			console.log('minTimeBetweenWagers Regulation - Additional Spin Duration Calculated:', extraRegulationDuration);
		}

		await new Promise<void>((resolve: () => void): void => {
			const controllerComplete = function (controller: ReelControllerSpinning): void {
				activeControllers.splice(activeControllers.indexOf(controller), 1);

				if (activeControllers.length === 0) {
					resolve();
				}
			};

			activeControllers.map((controller) => {
				const extraReelDuration: number = this.settings.reels.spinDurationExtraPerReel * controller.reelIndex;

				controller.reelSpinningDuration = this.settings.reels.spinDuration + extraReelDuration + extraRegulationDuration;
				controller.events.once(ReelControllerEvents.controllerCompleteEvent, controllerComplete);
				controller.spin(spinConfig.reelResults[controller.reelIndex]);
			});
		});
	}

	protected showWinsAll(message: IMessage): Promise<void> {
		return new Promise((resolve: () => void): void => {
			if (this.shouldSkipWinHighlights) {
				resolve();
			} else {
				this.messageBroker.getTopic(SHOW_WINLINES).publishAsync({ data: message.data });

				const complete = (): void => {
					this.skipWinFunction = undefined;
					this.skipWinTimer = undefined;
					resolve();
				};

				this.skipWinFunction = complete;
				this.skipWinTimer = this.time.setTimeout(this.skipWinFunction, this.settings.winlines.showAllDuration);
			}
		});
	}

	protected async showWinCycle(message: IMessage): Promise<void> {
		const wins: IWin[] = message.data.wins;
		let cycleIndex = 0;
		let cycling = !this.shouldSkipWinHighlights;
		let firstCycle = true;

		while (cycling) {
			const win: IWin = wins[cycleIndex];

			if (win.winType === WinType.Payline && typeof win.lineIndex === 'number') {
				setLineWinText(this.injector, win.totalWin, win.lineIndex);
			} else if (win.winType === WinType.Scatter) {
				setScatterWinText(this.injector, win.totalWin);
			}

			if (wins.length > 1 || (wins.length === 1 && firstCycle)) {
				this.messageBroker.getTopic(SHOW_WINLINE).publishAsync({
					topic: SHOW_WINLINE,
					data: win,
				});
			}

			await this.messageBroker.getTopic(SHOW_WIN_ITERATION).publish({
				topic: SHOW_WIN_ITERATION,
				data: win,
			});

			cycling = !this.shouldSkipWinHighlights && (!!message.data.loop || cycleIndex + 1 < wins.length);
			cycleIndex = (cycleIndex + 1) % wins.length;
			firstCycle = false;
		}

		if (!message.data.loop) {
			this.shouldSkipWinHighlights = false;
		}
	}

	protected showWinIteration(): Promise<void> {
		return new Promise((resolve) => {
			this.reelSet.stopAllAnimations();

			const evaluationComplete = (): void => {
				this.skipWinFunction = undefined;
				this.skipWinTimer = undefined;
				resolve();
			};

			this.skipWinFunction = evaluationComplete;
			this.skipWinTimer = this.time.setTimeout(this.skipWinFunction, this.settings.winlines.cycleDuration);
		});
	}

	protected async skipWin(message: IMessage): Promise<void> {
		if (message.topic !== GameUITopics.REQUEST_SPIN) {
			setSpinReady(this.injector);
		}

		this.shouldSkipWinHighlights = true;

		this.reelSet.stopAllAnimations();

		const skipWinTimer = this.skipWinTimer;
		const skipWinFunction = this.skipWinFunction;

		this.skipWinFunction = undefined;
		this.skipWinTimer = undefined;

		if (skipWinTimer) {
			this.time.clearTimeout(skipWinTimer);
		}
		if (skipWinFunction) {
			skipWinFunction();
		}
	}

	protected createReelPositionKey(position: ISymbolPosition): string {
		return `position:{${position.reelIndex},${position.symbolIndex}}`;
	}

	private tempCreateIndexOrder(): number[][] {
		const { boundarySymbolsBottom, boundarySymbolsTop, numberOfReels, numberOfRows } = this.settings.reels;

		return Array(numberOfReels)
			.fill(null)
			.map(() =>
				Array(numberOfRows + boundarySymbolsBottom + boundarySymbolsTop)
					.fill(null)
					.map((_, i) => i)
					.reverse()
			);
	}

	protected getReelSetParams(): ReelSetParams {
		const grid: IPoint[][] = createSlotGrid(this.settings.reels);

		// Sets z index for each symbol position / type. This should be customised in the settings.json...
		const symbolOrderConfig: ISymbolOrderConfig = {
			indexOrderConfig: this.tempCreateIndexOrder(),
			nameOrderConfig: new Map(this.settings.reels.nameOrderConfig),
		};

		return {
			grid: grid,
			staticConfigs: new Map<string, ISpriteGraph | ISpineConfig>(this.scaleStaticConfigs(this.settings.reels.staticConfigs)),
			textureRetriever: this.assetManager.getAsset.bind(this.assetManager),
			animationRetriever: this.getAnimation.bind(this),
			animationManager: this.animationManager,
			assetManager: this.assetManager,
			symbolOrderConfig: symbolOrderConfig,
			container: this.container,
		};
	}

	protected scaleStaticConfigs(configInput: [string, ISpriteGraph | ISpineConfig][]): [string, ISpriteGraph | ISpineConfig][] {
		configInput.forEach(([key, value]) => {
			if ('texture' in value) {
				this.scaleSpriteGraph(value);
			} else if ('json' in value) {
				this.scaleSpineConfig(value);
			} else {
				console.error('Could not scale static config %s, required fields not found: %o', key, value);
			}
		});

		return configInput;
	}

	protected scaleSpriteGraph(config: ISpriteGraph): void {
		if (config.children) {
			config.children.map(this.scaleSpriteGraph, this);
		}
		if (config.pivot) {
			config.pivot.x = this.bucketScale * config.pivot.x;
			config.pivot.y = this.bucketScale * config.pivot.y;
		}
		if (config.position) {
			config.position.x = this.bucketScale * config.position.x;
			config.position.y = this.bucketScale * config.position.y;
		}
	}

	protected scaleSpineConfig(config: ISpineConfig): void {
		if (config.transform) {
			if (config.transform.pivot) {
				config.transform.pivot.x = this.bucketScale * config.transform.pivot.x;
				config.transform.pivot.y = this.bucketScale * config.transform.pivot.y;
			}
			if (config.transform.position) {
				config.transform.position.x = this.bucketScale * config.transform.position.x;
				config.transform.position.y = this.bucketScale * config.transform.position.y;
			}
		}
	}

	protected onSymbolLanded(): void {}

	protected onReelLanded(): void {}

	protected getControllerConfig(reelIndex: number): IReelControllerSpinningConfig {
		let reelStripSymbols: string[] = [];

		if (this.reelStripsConfigurations != null && this.currentReelStrip.state != null) {
			const reelStripConfig: IReelStrips | undefined = this.reelStripsConfigurations.get(this.currentReelStrip.state);
			reelStripSymbols = reelStripConfig?.reelStrips[reelIndex]?.symbols ?? [];
		}

		return {
			animationManager: this.animationManager,
			reelSet: this.reelSet,
			reelIndex: reelIndex,
			baseSymbolWidth: this.baseSymbolWidth,
			baseSymbolHeight: this.baseSymbolHeight,
			reelStrip: reelStripSymbols,
		};
	}

	protected async setReelMaps(message: IMessage): Promise<void> {
		this.reelStripsConfigurations = message.data.reelStripsConfigurations;
		this.currentReelStrip.state = message.data.currentReelStrip;
	}

	protected changeReelStrip(): void {
		if (this.reelStripsConfigurations != null && this.currentReelStrip.state != null) {
			const reelStripConfig: IReelStrips | undefined = this.reelStripsConfigurations.get(this.currentReelStrip.state);

			if (reelStripConfig) {
				for (const controller of this.reelControllers) {
					const reelStrip = reelStripConfig.reelStrips[controller.reelIndex];
					controller.reelStrip.state = reelStrip.symbols;
				}
			}
		}
	}

	protected createReelMask(): Graphics {
		const reelsWidth = this.baseSymbolWidth * this.settings.reels.numberOfReels;
		const reelsHeight = this.baseSymbolHeight * this.settings.reels.numberOfRows;

		const mask = new Graphics();
		mask.beginFill(0xFFFFFF);
		mask.drawRect(reelsWidth * -0.5, reelsHeight * -0.5, reelsWidth, reelsHeight);
		mask.endFill();

		return mask;
	}

	protected getAnimation(symbol: ReelSymbol, animationName: string): SymbolAnimation {
		// Return the required animation for a given symbol and win type here...
		// Currently just returns an 'empty' animation.
		return {
			animation: this.animationManager.tween(symbol.container, { tweens: [{ duration: 0, target: {} }] }),
			hideStatic: false,
		};
	}

	protected update(deltaMS: number): void {
		this.reelControllers.map((controller) => {
			controller.updateTick(deltaMS);
		});
	}
}
