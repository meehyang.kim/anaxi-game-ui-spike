/**
 * Controls the show/hide of winlines when showing all/iterating wins.
 */

import { IPoint, ViewComponent, ViewConstructorProps } from '@roxorgaming/bolt-display';
import {
	ISymbolPosition,
	IWinlinesGraphicsOptions,
	WinlinesGraphics,
	IBoxConfig,
	IBoxOptions,
	ICustomLine,
	IGraphicsLineLayer,
	IWinBoxConfig,
} from '@roxorgaming/bolt-slots-reels';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { ENDED_WINS_ALL, ENDED_WIN_CYCLE, SHOW_WINLINE, SHOW_WINLINES } from '@roxorgaming/bolt-slots-shared';
import { StoreKeys } from '@roxorgaming/bolt-loading-sequence';
import { ReadableStore } from '@roxorgaming/bolt-store';
import { IWin, WinType } from '@roxorgaming/bolt-slots-communication';
import { GameSettings } from '../settings';
import { GameStoreModel } from '../store/types';
import { createSlotGrid } from '../utils';

export class WinlinesViewComponent extends ViewComponent {
	protected readonly settings: GameSettings;
	protected readonly scale: number;

	protected readonly lineColourMap: Map<string, string>;
	protected readonly winlinesBase: WinlinesGraphics;
	protected readonly winlinesTop: WinlinesGraphics;

	constructor(props: ViewConstructorProps) {
		super(props);

		const store: ReadableStore<GameStoreModel> = this.injector.get(ReadableStore);

		this.settings = store.getState(StoreKeys.SETTINGS) as GameSettings;
		this.scale = window.gcore.display.bucket.getScale();
		this.lineColourMap = new Map<string, string>(this.settings.winlines.symbolLineColors);

		const reelGrid: IPoint[][] = createSlotGrid(this.settings.reels);

		const winlineSymbolPositions: IPoint[][] = [];
		for (const reelPositions of reelGrid) {
			const linePositions: IPoint[] = [];
			winlineSymbolPositions.push(linePositions);
			for (let symbolIndex = 0; symbolIndex < 3; ++symbolIndex) {
				const linePosition: IPoint = reelPositions[symbolIndex + this.settings.reels.boundarySymbolsTop];
				linePositions.push({ ...linePosition });
			}
		}

		const winlinesOptionsBase: IWinlinesGraphicsOptions = {
			parent: this.container,
			symbolPositions: winlineSymbolPositions,
			lineCoordinates: this.settings.winlines.positions,
			lineConfig: this.scaleGraphicsLineLayer(this.settings.winlines.base),
			maxTextureSize: undefined,
		};

		const winlinesOptionsTop: IWinlinesGraphicsOptions = {
			parent: this.container,
			symbolPositions: winlineSymbolPositions,
			lineCoordinates: this.settings.winlines.positions,
			lineConfig: this.scaleGraphicsLineLayer(this.settings.winlines.top),
			maxTextureSize: undefined,
		};

		this.winlinesBase = new WinlinesGraphics(winlinesOptionsBase);
		this.winlinesTop = new WinlinesGraphics(winlinesOptionsTop);
	}

	public build(): void {
		this.messageBroker.getTopic(SHOW_WINLINES).subscribe(this.showWinAll.bind(this), WinlinesViewComponent.name);
		this.messageBroker.getTopic(SHOW_WINLINE).subscribe(this.showWinIteration.bind(this), WinlinesViewComponent.name);
		this.messageBroker.getTopic(ENDED_WINS_ALL).subscribe(this.hideWinlines.bind(this), WinlinesViewComponent.name);
		this.messageBroker.getTopic(ENDED_WIN_CYCLE).subscribe(this.hideWinlines.bind(this), WinlinesViewComponent.name);
	}

	protected showWinAll(message: IMessage): Promise<void> {
		const wins: IWin[] = message.data.wins;
		const linesTop: (number | ICustomLine)[] = [];
		const linesBase: (number | ICustomLine)[] = [];
		const boxConfigs: { box: IBoxOptions; positions: ISymbolPosition[] }[] = [];

		this.winlinesBase.hide();
		this.winlinesTop.hide();

		wins.map((win) => {
			if (win.winType === WinType.Payline && typeof win.lineIndex === 'number' && win.symbols) {
				const color: string | undefined = this.lineColourMap.get(win.symbols[0]);

				linesTop.push(win.lineIndex - 1);
				linesBase.push({
					index: win.lineIndex - 1,
					strokeColor: color,
					shadowColor: color,
				});
			} else if (this.settings.winlines.scatterAllWinsShowBox && win.winType === WinType.Scatter && win.positions && win.positions?.length > 0 && win.symbols) {
				const lineSymbolPositions: ISymbolPosition[] = win.positions.map((position) => ({ reelIndex: position.x, symbolIndex: position.y }));
				const color: string | undefined = this.lineColourMap.get(win.symbols[0]);

				boxConfigs.push({
					box: {
						boxStrokeColor: color,
						boxShadowColor: color,
						boxClearWindow: this.settings.winlines.scatterAllWinsClearBoxWindow,
					},
					positions: lineSymbolPositions,
				});
			}
		});

		this.winlinesBase.show(...linesBase);
		this.winlinesTop.show(...linesTop);

		for (const boxConfig of boxConfigs) {
			this.winlinesBase.showBoxes(boxConfig.box, ...boxConfig.positions);
			this.winlinesTop.showBoxes({ boxClearWindow: boxConfig.box.boxClearWindow }, ...boxConfig.positions);
		}

		return Promise.resolve();
	}

	protected showWinIteration(message: IMessage): Promise<void> {
		const win: IWin = message.data;
		const winningReelPositions: ISymbolPosition[] = [];

		this.winlinesBase.hide();
		this.winlinesTop.hide();

		if (win.winType === WinType.Payline && typeof win.lineIndex === 'number' && win.symbols) {
			const color: string | undefined = this.lineColourMap.get(win.symbols[0]);

			const boxes: number[] = [];
			for (let count = 0; count < (win.symbolsCount ?? 0); ++count) {
				boxes.push(count);
			}

			const lineTop: ICustomLine = {
				index: win.lineIndex - 1,
				boxes: boxes,
			};
			const lineBase: ICustomLine = {
				index: win.lineIndex - 1,
				strokeColor: color,
				shadowColor: color,
				boxStrokeColor: color,
				boxShadowColor: color,
				boxes: boxes,
			};

			this.winlinesBase.show(lineBase);
			this.winlinesTop.show(lineTop);
		} else if (this.settings.winlines.scatterIterateShowBox && win.winType === WinType.Scatter && win.positions && win.positions?.length > 0 && win.symbols) {
			const lineSymbolPositions: ISymbolPosition[] = win.positions.map((position) => {
				const symbolPosition: ISymbolPosition = { reelIndex: position.x, symbolIndex: position.y };
				winningReelPositions.push({ reelIndex: position.x, symbolIndex: position.y + this.settings.reels.boundarySymbolsTop });
				return symbolPosition;
			});

			const color: string | undefined = this.lineColourMap.get(win.symbols[0]);

			const optionsBox: IBoxOptions = {
				boxStrokeColor: color,
				boxShadowColor: color,
				boxClearWindow: this.settings.winlines.scatterIterateClearBoxWindow,
			};

			this.winlinesBase.showBoxes(optionsBox, ...lineSymbolPositions);
			this.winlinesTop.showBoxes({ boxClearWindow: optionsBox.boxClearWindow }, ...lineSymbolPositions);
		}

		return Promise.resolve();
	}

	protected hideWinlines(): Promise<void> {
		this.winlinesBase.hide();
		this.winlinesTop.hide();

		return Promise.resolve();
	}

	protected scaleGraphicsLineLayer(input: IGraphicsLineLayer): IGraphicsLineLayer {
		const outputBox: IBoxConfig | undefined = input.box
			? {
					windowSize: {
						width: input.box.windowSize.width * this.scale,
						height: input.box.windowSize.height * this.scale,
					},
					radius: input.box.radius ? input.box.radius * this.scale : undefined,
					strokeColor: input.box.strokeColor,
					lineWidth: input.box.lineWidth ? input.box.lineWidth * this.scale : undefined,
					shadowColor: input.box.shadowColor,
					shadowBlur: input.box.shadowBlur ? input.box.shadowBlur * this.scale : undefined,
					retainLine: input.box.retainLine,
			  }
			: undefined;

		const outputWinBox: IWinBoxConfig | undefined = input.winBox
			? {
					windowSize: {
						width: input.winBox.windowSize.width * this.scale,
						height: input.winBox.windowSize.height * this.scale,
					},
					position: input.winBox.position
						? {
								x: input.winBox.position.x ? input.winBox.position.x * this.scale : undefined,
								y: input.winBox.position.y ? input.winBox.position.y * this.scale : undefined,
						  }
						: undefined,
					radius: input.winBox.radius ? input.winBox.radius * this.scale : undefined,
					strokeColor: input.winBox.strokeColor,
					fillColor: input.winBox.fillColor,
					clearWindow: input.winBox.clearWindow,
					lineWidth: input.winBox.lineWidth ? input.winBox.lineWidth * this.scale : undefined,
					shadowColor: input.winBox.shadowColor,
					shadowBlur: input.winBox.shadowBlur ? input.winBox.shadowBlur * this.scale : undefined,
			  }
			: undefined;

		const output: IGraphicsLineLayer = {
			strokeColor: input.strokeColor,
			lineWidth: input.lineWidth * this.scale,
			extendLength: input.extendLength ? input.extendLength * this.scale : undefined,
			isCurved: input.isCurved,
			shadowColor: input.shadowColor,
			shadowBlur: input.shadowBlur ? input.shadowBlur * this.scale : undefined,
			box: outputBox,
			winBox: outputWinBox,
			lineCap: input.lineCap,
			lineJoin: input.lineJoin,
		};

		return output;
	}
}
