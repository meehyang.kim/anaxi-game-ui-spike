/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/api/command-api/
 *
 * Here we have the handshake command queue. A command queue is essentially
 * a sequence of commands that gets run sequentially. For instance the command queue below
 * describes the sequence of events that happens with a handshake:
 * 1. we request a handshake (from the server)
 * 2. once we got the result from the server, we would
 * configure the GameUI (with coinsize, balance etc from the handshake)
 * 3. setup the game layout
 * 4. show the base game visual state
 * 5. show the Game UI
 */
import { CommandQueue } from '@roxorgaming/bolt-command';
import { initialiseGameBusy, showGameUI, setGameLayout, setReelMaps, setInitialReels } from '@roxorgaming/bolt-slots-shared';
import { requestHandshake, showBaseGame, configureGameUI, removeIntroLayout } from '../commands';

export function buildHandshakeCommandQueue(): CommandQueue {
	const queue = new CommandQueue();
	queue
		.add(requestHandshake)
		.add(configureGameUI)
		.add(removeIntroLayout)
		.add(setGameLayout)
		.add(setReelMaps)
		.add(setInitialReels)
		.add(showBaseGame)
		.add(showGameUI)
		.add(initialiseGameBusy);
	return queue;
}
