/**
 * Roxor GDK:
 * https://github.gamesys.co.uk/pages/game-studios-core/roxor-gdk/docs/coding-frontend/bolt/api/command-api/
 *
 * Here we have the spin command queue. A command queue is essentially
 * a sequence of commands that gets run sequentially.
 * Each command has an explanation in it's own file.
 */

import { CommandQueue } from '@roxorgaming/bolt-command';
import {
	checkGameplayFinishedBaseSpin,
	checkGameplayStarted,
	clearAllWins,
	clearWinCycle,
	setPostSpinBalanceBaseSpin,
	setPostWagerBalance,
	spinReels,
	setReelMaps,
	waitForLanding,
} from '@roxorgaming/bolt-slots-shared';
import { checkShowAllWins, checkShowWinCycle, handleEndOfSpin } from '../commands';

export function buildSpinCommandQueue(): CommandQueue {
	const queue = new CommandQueue();
	queue
		.add(setPostWagerBalance)
		.add(checkGameplayStarted)
		.add(setReelMaps)
		.add(spinReels)
		.add(waitForLanding)
		.add(setPostSpinBalanceBaseSpin)
		.add(checkGameplayFinishedBaseSpin)
		.add(checkShowAllWins)
		.add(clearAllWins)
		.add(handleEndOfSpin)
		.add(checkShowWinCycle)
		.add(clearWinCycle);
	return queue;
}
