/**
 * This is where you would register all your CommandQueues to Topics
 * In other words, you are defining what command queue should be
 * triggered when which Topic is published.
 * eg.
 * // When REQUEST_HANDSHAKE Topic is published, buildHandshakeCommandQueue will be triggered
 * controller.registerCommandQueueToTopic(REQUEST_HANDSHAKE, buildHandshakeCommandQueue());
 *
 */
import { IInjector } from '@roxorgaming/bolt-injector';
import { IMessageBroker } from '@roxorgaming/bolt-message-broker';
import { CommandController } from '@roxorgaming/bolt-command';
import { GameUITopics } from '@roxorgaming/bolt-loading-sequence';
import { REQUEST_HANDSHAKE, TRIGGER_SPIN_QUEUE } from '@roxorgaming/bolt-slots-shared';
import { requestSpin } from './commands';
import { buildHandshakeCommandQueue, buildSpinCommandQueue } from './queues';

export function buildCommandController(injector: IInjector, messageBroker: IMessageBroker): CommandController {
	const controller = new CommandController(injector, messageBroker);
	controller.registerCommandQueueToTopic(REQUEST_HANDSHAKE, buildHandshakeCommandQueue());
	controller.registerCommandToTopic(GameUITopics.REQUEST_SPIN, requestSpin);
	controller.registerCommandQueueToTopic(TRIGGER_SPIN_QUEUE, buildSpinCommandQueue());
	/* register command(Queue) here */

	return controller;
}
