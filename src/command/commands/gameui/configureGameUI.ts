/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/game-ui/layout-api/
 *
 * Here we setup the Game UI with data we obtained from the handshake.
 *
 */
import { SlotResponse } from '@roxorgaming/bolt-slots-communication';
import { IInjector } from '@roxorgaming/bolt-injector';
import { SlotGameUI } from '@roxorgaming/bolt-loading-sequence';
import { StoreKeys } from '@roxorgaming/bolt-slots-shared';
import { WritableStore } from '@roxorgaming/bolt-store';
import { getCurrency } from '@roxorgaming/bolt-utils';
import { BoltLocaliser } from '@roxorgaming/bolt-localisation';

export async function configureGameUI(injector: IInjector): Promise<void> {
	const store = injector.get(WritableStore);
	const response = store.getState(StoreKeys.RESPONSE) as SlotResponse;
	const gameui = injector.get(SlotGameUI);
	const layout = gameui.layout;
	const gameProperties = window.gcore.getCommon().gameProperties;
	const formatter = window.gcore.getCommon().currencyFormatter;

	const localiser = injector.get(BoltLocaliser);
	layout.text.message.set(localiser.getLocalisedText({ key: 'PRESS_SPIN_TO_PLAY!' }));

	if (response.coinSizes) {
		const coinSizes = response.coinSizes.coinSizes.map((size) => {
			const label: string = formatter.format(
				size,
				gameProperties.currency,
				gameProperties.playMode,
				{ useSubUnits: true, splitThousands: true, addTrailingZeros: false },
				gameProperties.locale
			);
			return { label: label, data: size };
		});

		// sets the range of coinSizes available for this game
		layout.coinSize.sizes.set(coinSizes);

		// sets the current/default coinSize
		layout.coinSize.index.set(response.coinSizes.coinSizes.indexOf(response.coinSizes.currentCoinSize ?? response.coinSizes.defaultCoinSize));
	}

	// sets the number of Coins your game uses
	layout.coins.configure({ value: Number(store.getState(StoreKeys.COINS)) });

	if (response.bets && response.coinSizes) {
		const currentBets = response.bets.currentBets;
		const coinSize = response.coinSizes.currentCoinSize ?? response.coinSizes.defaultCoinSize;
		const formattedCoinSize = formatter.format(
			coinSize,
			gameProperties.currency,
			gameProperties.playMode,
			{ useSubUnits: true, splitThousands: true, addTrailingZeros: false },
			gameProperties.locale
		);
		const formattedTotal = formatter.format(
			currentBets * coinSize,
			gameProperties.currency,
			gameProperties.playMode,
			{ useSubUnits: true, splitThousands: true, addTrailingZeros: false },
			gameProperties.locale
		);

		// Sets the bet field: Bet 25 x 4p = £1
		layout.text.bet.set(`${currentBets} x ${formattedCoinSize} = ${formattedTotal}`);
	}

	// if your game supports bet lines you can set the number of lines
	// on Game UI like this
	if (response.bets) {
		const betLineConfig = {
			lines: [response.bets.availableBetsArray.length],
			index: 0,
		};
		layout.betLine.configure(betLineConfig);
	}

	if (response.balance) {
		// updates the balance
		store.setState(StoreKeys.BALANCE, response.balance.preWagerBalance.amount, true);
		layout.text.balance.set(getCurrency(response.balance.preWagerBalance.amount));
		window.gcore.getCommon().gameBridge.setBalance(response.balance.preWagerBalance.amount);
	}

	if (response.transactionId) {
		layout.text.transactionID.set(response.transactionId);
	}
}
