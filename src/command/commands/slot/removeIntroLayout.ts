/**
 * After loading is complete we never need to show intro screen again.
 */

import { IMessage } from '@roxorgaming/bolt-message-broker';
import { LAYOUT_REMOVE_LAYOUT } from '@roxorgaming/bolt-slots-shared';
import { LayoutIntro } from '../../../constants';

export async function removeIntroLayout(): Promise<IMessage> {
	return {
		topic: LAYOUT_REMOVE_LAYOUT,
		data: {
			layout: LayoutIntro,
		},
	};
}
