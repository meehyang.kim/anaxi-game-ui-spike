/**
 * Sequence of iterating each win.
 * Sequence can be skipped with new spinRequest or wager change.
 * Any game-specific win display logic can be added here.
 */

import { IInjector } from '@roxorgaming/bolt-injector';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { StoreKeys } from '@roxorgaming/bolt-loading-sequence';
import { WritableStore } from '@roxorgaming/bolt-store';
import { SHOW_WIN_CYCLE } from '@roxorgaming/bolt-slots-shared';
import { GameSlotResponse } from '../../../comms/model';
import { GameStoreModel } from '../../../store/types';

export async function checkShowWinCycle(injector: IInjector): Promise<IMessage | void> {
	const store: WritableStore<GameStoreModel> = injector.get(WritableStore);
	const response: GameSlotResponse = store.getState(StoreKeys.RESPONSE) as GameSlotResponse;
	const isAutoPlay = store.getState(StoreKeys.AUTOPLAY);

	if (response.wins && response.wins.length > 0 && !isAutoPlay) {
		return {
			topic: SHOW_WIN_CYCLE,
			data: {
				wins: response.wins,
				loop: true,
			},
		};
	}
}
