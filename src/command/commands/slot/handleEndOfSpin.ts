/**
 * Waits for minTimeBetweenWagers regulation timout before enabling spin buttons.
 */

import { StoreKeys, requestAutoSpin, setGameNotBusy } from '@roxorgaming/bolt-slots-shared';
import { IInjector } from '@roxorgaming/bolt-injector';
import { WritableStore } from '@roxorgaming/bolt-store';
import { MinWagerTimeout } from '@roxorgaming/bolt-utils';
import { stopSpin } from '@roxorgaming/bolt-utils';
import { GameSlotResponse } from '../../../comms/model';
import { GameStoreModel } from '../../../store/types';
import { setSpinReady } from '../../../utils';

export async function handleEndOfSpin(injector: IInjector): Promise<void> {
	const store: WritableStore<GameStoreModel> = injector.get(WritableStore);
	const response = store.getState(StoreKeys.RESPONSE) as GameSlotResponse;
	const minWagerTimeout = injector.get(MinWagerTimeout);

	stopSpin();

	await minWagerTimeout.complete;

	const isAutoPlay = store.getState(StoreKeys.AUTOPLAY);

	store.setState(StoreKeys.SPINNING, false, true);

	if (isAutoPlay) {
		requestAutoSpin(injector);
	} else {
		setGameNotBusy(injector);

		if (!response.wins?.length) {
			setSpinReady(injector);
		}
	}
}
