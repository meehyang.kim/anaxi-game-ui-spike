/**
 * Shows the base game.
 * This demonstrates a very basic Command.
 * All it does is it resolves the promise with an IMessage.
 * The IMessage gets published via the messageBroker.
 */
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { LAYOUT_SET_LAYOUT_VISIBLE } from '@roxorgaming/bolt-slots-shared';

export async function showBaseGame(): Promise<IMessage> {
	return {
		topic: LAYOUT_SET_LAYOUT_VISIBLE,
		data: {
			name: 'base-game',
			visibility: true,
			hideOthers: true,
		},
	};
}
