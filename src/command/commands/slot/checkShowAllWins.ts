/**
 * Show total win summary with all winlines.
 *
 * Any game-specific win display logic can be added here.
 */

import { IInjector } from '@roxorgaming/bolt-injector';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { WritableStore } from '@roxorgaming/bolt-store';
import { StoreKeys } from '@roxorgaming/bolt-loading-sequence';
import { SHOW_WINS_ALL, setWinText } from '@roxorgaming/bolt-slots-shared';
import { GameSlotResponse } from '../../../comms/model';
import { GameStoreModel } from '../../../store/types';

export async function checkShowAllWins(injector: IInjector): Promise<IMessage | void> {
	const store: WritableStore<GameStoreModel> = injector.get(WritableStore);
	const response: GameSlotResponse = store.getState(StoreKeys.RESPONSE) as GameSlotResponse;

	if (response.wins && response.wins.length > 0) {
		const grossWin = response.grossWin;

		if (grossWin !== null && grossWin > 0) {
			setWinText(injector, grossWin, {
				useSubUnits: true,
				splitThousands: true,
				addTrailingZeros: true,
			});
		}

		return {
			topic: SHOW_WINS_ALL,
			data: {
				wins: response.wins,
			},
		};
	}
}
