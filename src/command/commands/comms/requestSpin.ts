/**
 * Roxor GDK:
 * https://github.gamesys.co.uk/pages/game-studios-core/roxor-gdk/docs/coding-frontend/bolt/api/store-api/
 *
 * Here we save the spin response to the store. You have a choice between a
 * flat key/ value store or a more structured model storage. The Bolt Store supports both styles
 */

import { IInjector } from '@roxorgaming/bolt-injector';
import { CommunicationController } from '@roxorgaming/bolt-communication';
import { RequestType } from '@roxorgaming/bolt-communication';
import { WritableStore } from '@roxorgaming/bolt-store';
import { SlotGameUI, SlotLayoutEvents } from '@roxorgaming/bolt-loading-sequence';
import { StoreKeys } from '@roxorgaming/bolt-slots-shared';
import { GameSlotResponse } from '../../../comms/model';
import { GameStoreModel } from '../../../store/types';
import { BoltTime, MinWagerTimeout } from '@roxorgaming/bolt-utils';
import { TRIGGER_FREE_SPIN_QUEUE, TRIGGER_SPIN_QUEUE } from '@roxorgaming/bolt-slots-shared';
import { setSpinReady } from '../../../utils';
import { INextSpinState } from '@roxorgaming/bolt-slots-communication';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { BoltLocaliser } from '@roxorgaming/bolt-localisation';

function getNextEvent(nextSpinState: INextSpinState | null): string {
	const inFreeSpins = nextSpinState?.spinState == 'FREE_SPIN' || nextSpinState?.spinState == 'FREE_CASCADE';
	const inCascades = nextSpinState?.spinState == 'CASCADE_SPIN' || nextSpinState?.spinState == 'FREE_CASCADE';

	return inFreeSpins ? (inCascades ? 'FreeCascade' : 'FreeSpin') : inCascades ? 'Cascade' : 'Spin';
}

function getNumberOfCoins(numberBets: number, availableBets: number): string {
	const currentBetsArray: number[] = [];

	for (let i = 0; i < availableBets; ++i) {
		currentBetsArray.push(i < numberBets ? 1 : 0);
	}

	return currentBetsArray.join(',');
}

export async function requestSpin(injector: IInjector): Promise<IMessage> {
	const store: WritableStore<GameStoreModel> = injector.get(WritableStore);
	const comms = injector.get(CommunicationController);
	const localiser = injector.get(BoltLocaliser);
	const gameUI = injector.get(SlotGameUI);
	const boltTime = injector.get(BoltTime);
	const settings = store.getState(StoreKeys.SETTINGS);
	const availableBets = store.getState(StoreKeys.COINS);
	const currentBets = store.getState(StoreKeys.CURRENT_BETS);

	store.setState(StoreKeys.SPINNING, true, true);

	const payload = {
		coinSize: '' + store.getState(StoreKeys.COIN_SIZE),
		numberOfCoins: getNumberOfCoins(currentBets, availableBets),
	};
	const previousResponse = store.getState(StoreKeys.RESPONSE);
	const event = getNextEvent(previousResponse.nextSpinState);
	console.debug('requestSpin payload:', payload);
	injector.get(MinWagerTimeout).start();
	gameUI.layout.text.message.set(localiser.getLocalisedText({ key: 'GOOD_LUCK!' }));
	gameUI.layout.text.win.set('');
	window.gcore.getCommon().gameBridge.gameBusy();

	let showedWaitMessage = false;
	const showWaitFunction = (): void => {
		showedWaitMessage = true;
		gameUI.layout.text.message.set(localiser.getLocalisedText({ key: 'WAITING_FOR_SERVER_RESPONSE' }));
	};
	const waitServerTimeout = boltTime.setTimeout(showWaitFunction, settings.general.waitServerTimeout);

	let message = {
		topic: event.toUpperCase() === 'SPIN' ? TRIGGER_SPIN_QUEUE : event.toUpperCase() === 'FREESPIN' ? TRIGGER_FREE_SPIN_QUEUE : '',
	};

	await comms
		.requestResponse(event, JSON.stringify(payload), RequestType.CallGame, true)
		.then((response: GameSlotResponse) => {
			console.debug('spin response: ', response);
			store.setState(StoreKeys.RESPONSE, response, true);

			if (waitServerTimeout) {
				boltTime.clearTimeout(waitServerTimeout);
			}
			if (showedWaitMessage) {
				gameUI.layout.text.message.set(localiser.getLocalisedText({ key: 'GOOD_LUCK!' }));
			}
		})
		.catch(() => {
			if (waitServerTimeout) {
				boltTime.clearTimeout(waitServerTimeout);
			}
			store.setState(StoreKeys.SPINNING, false, true);
			setSpinReady(injector);

			message = { topic: '' };

			if (window.gcore.insufficientFunds.isShowing) {
				gameUI.layout.spin.spinComplete();
				window.gcore.getCommon().gameBridge.gameNotBusy();

				if (store.getState(StoreKeys.AUTOPLAY)) {
					message = { topic: SlotLayoutEvents.AUTO_PLAY_STOP };
				}
			}
		});
	return message;
}
