/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/api/store-api/
 *
 * Here we save the handshake response to the store. You have a choice between a
 * flat key/ value store or a more structured model storage. The Bolt Store supports both styles
 */
import { CommunicationController } from '@roxorgaming/bolt-communication';
import { IMessage } from '@roxorgaming/bolt-message-broker';
import { RequestType } from '@roxorgaming/bolt-communication';
import { IInjector } from '@roxorgaming/bolt-injector';
import { StoreKeys, REQUEST_HANDSHAKE_SUCCESS } from '@roxorgaming/bolt-slots-shared';
import { WritableStore } from '@roxorgaming/bolt-store';
import { GameSlotResponse } from '../../../comms/model';
import { GameStoreModel } from '../../../store/types';

export async function requestHandshake(injector: IInjector): Promise<IMessage> {
	const store: WritableStore<GameStoreModel> = injector.get(WritableStore);
	const comms = injector.get(CommunicationController);

	const response: GameSlotResponse = await comms.requestResponse('Handshake', '{}', RequestType.CallGame, true);
	store.setState(StoreKeys.RESPONSE, response, true);

	if (response.coinSizes) {
		if (response.coinSizes.currentCoinSize) {
			store.setState(StoreKeys.COIN_SIZE, response.coinSizes.currentCoinSize, true);
		} else {
			store.setState(StoreKeys.COIN_SIZE, response.coinSizes.defaultCoinSize, true);
		}
	}
	if (response.bets) {
		store.setState(StoreKeys.COINS, response.bets.availableBets, true);
		store.setState(StoreKeys.CURRENT_BETS, response.bets.currentBets, true);
		store.setState(StoreKeys.CURRENT_BETS_ARRAY, response.bets.currentBetsArray, true);
	}

	if (response.balance) {
		store.setState(StoreKeys.BALANCE, response.balance.postWagerBalance.amount, true);
	}

	console.debug(response);

	return {
		topic: REQUEST_HANDSHAKE_SUCCESS,
		data: response,
	};
}
