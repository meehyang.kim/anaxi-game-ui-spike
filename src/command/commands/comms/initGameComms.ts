/**
 * Roxor GDK:
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/api/communication-api/
 * https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/api/slots-communication-api/
 *
 * Here we're initialising the interpreter and add all of the Parsers used for this game
 * or this type of game eg. Slots, DFG, Casino will use slightly different Parsers
 *
 */
import { IInjector } from '@roxorgaming/bolt-injector';
import { CommunicationController, ResponseInterpreter } from '@roxorgaming/bolt-communication';
import {
	ErrorCheckerSCJson,
	PlayerBalanceInfoParser,
	GameWageringInfoParser,
	NextActionInfoParser,
	GameReelInfoParser,
	GameSlotResultInfoParser,
	RawResponseConverterSCJson,
	GameReferenceInfoParser,
	ResponseSCJson,
} from '@roxorgaming/bolt-slots-communication';
import { GameResponseSCJson, GameSlotResponse } from '../../../comms/model';

export async function initGameComms(injector: IInjector): Promise<void> {
	const controller: CommunicationController<string, ResponseSCJson, GameSlotResponse> = injector.get(CommunicationController);
	controller.rawConverter = new RawResponseConverterSCJson();

	const interpreter = new ResponseInterpreter<GameResponseSCJson, GameSlotResponse>(GameSlotResponse);
	interpreter.addErrorChecker(new ErrorCheckerSCJson(window.gcore.getCommon().gameBridge));
	interpreter.addParser(new PlayerBalanceInfoParser());
	interpreter.addParser(new GameWageringInfoParser());
	interpreter.addParser(new GameReferenceInfoParser());
	interpreter.addParser(new NextActionInfoParser());
	interpreter.addParser(new GameReelInfoParser());
	interpreter.addParser(new GameSlotResultInfoParser());
	// Add custom game response parsers here

	controller.addInterpreter(interpreter);
}
