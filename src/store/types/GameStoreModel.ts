/**
 * The game model definition
 */

import { StoreModel } from '@roxorgaming/bolt-slots-shared';
import { GameSettings } from '../../settings';
import { GameSlotResponse } from '../../comms';

export enum GameStoreKeys {}
// Add game specific store keys here

export type GameStoreModel = StoreModel<GameSettings, GameSlotResponse> & {
	// Add game specific store properties here
};
