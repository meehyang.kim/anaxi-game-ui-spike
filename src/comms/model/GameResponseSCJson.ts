/**
 * The backend response structure definition
 */
import { ResponseSCJson } from '@roxorgaming/bolt-slots-communication';

export type GameResponseSCJson = ResponseSCJson & {
	//TODO: ADD GAME SPECIFIC BACKEND RESPONSE TYPINGS HERE
};
