/**
 * The parsed backend response used by the game
 */
import { SlotResponse } from '@roxorgaming/bolt-slots-communication';

export class GameSlotResponse extends SlotResponse {
	//TODO: ADD GAME SPECIFIC PARSED RESPONSE FIELDS HERE
}
