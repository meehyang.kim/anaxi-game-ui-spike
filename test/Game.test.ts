import { Game } from '../src/Game';
import { IGCore } from '@roxorgaming/types-gcore';
import { IGSCommon } from '@roxorgaming/types-rgp-common';

describe('Game', () => {
    window.gcore = {
        getCommon: () => {
            return ({
                gameProperties: {
                    centralisedMessages: false,
                },
                deviceInfo: {
                    PORTRAIT: 'PORTRAIT',
                    orientation: 'LANDSCAPE'
                }
            } as unknown) as IGSCommon;
        },
        display: {
            setGamePadding: jest.fn(),
            bucket: {
                setBucket: jest.fn(),
            }
        },
        regulations: {
            minTimeBetweenWagers: 3000
        }
    } as any as IGCore;

    let instance: Game;

    beforeEach(() => {
        instance = new Game();
    });

    test('start function exists', () => {
        expect(instance.start).not.toBeNull();
    });

});
