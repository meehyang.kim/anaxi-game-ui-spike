# Game Name Here

## Build

Production build command: `npm run build`

Development build command: `npm run build:dev`

Development build with fake backend: `npm run build:fakeBackend`


## Bolt Local

Runs the game using the local frontend game assets against either a real backend or a fake backend.

Instructions on how to setup Bolt Local can be found within the GDK
[here](https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/docs/coding-frontend/bolt/tools/bolt-local/).

Once Docker is setup, simply build the game and run: `npm run start:boltLocal`

If you are running in offline mode, using a fake backend you will need to run this command first: `npm run build:fakeBackend`
