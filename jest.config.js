const dependenciesToJestConfig = require('./node_modules/@roxorgaming/bolt-base/utils/dependenciesToJestConfig');
const { pathsToModuleNameMapper } = require('ts-jest');
const package = require('./package.json');

const pixiSpineDependency = package.config.pixiSpineDependency;
let jestConfig = require('./node_modules/@roxorgaming/bolt-base/configs/jest.config.js');
const dependenciesToJest = dependenciesToJestConfig(package.dependencies);
dependenciesToJest.globals = {
	...dependenciesToJest.globals,
	...jestConfig.globals,
};

jestConfig = Object.assign(jestConfig, dependenciesToJest); //add depdendencies as ts paths and module map to support npm link
jestConfig.testEnvironment = 'jsdom';
jestConfig.setupFiles = ['jest-canvas-mock', '<rootDir>/jest.setup.js'];
jestConfig.coverageThreshold = {
	global: {
		branches: 0,
		functions: 0,
		lines: 0,
		statements: 0,
	},
};
jestConfig.globals['ts-jest'].tsconfig.paths = {
	...jestConfig.globals['ts-jest'].tsconfig.paths,
	'pixi-spine-all': [`node_modules/${pixiSpineDependency}`],
};

jestConfig.moduleNameMapper = pathsToModuleNameMapper(jestConfig.globals['ts-jest'].tsconfig.paths, { prefix: '<rootDir>/' });

module.exports = jestConfig;
