const package = require('./package.json');
const webpackConfigurator = require('./node_modules/@roxorgaming/bolt-base/configs/game.webpack.config');

// proxy info for webpack dev server - allows us to load rgp-console locally with local game assets
const DEV_ENV_PROXY = 'cdn.games-dev.roxor.games'; //dev environment to proxy to

const isProduction = process.env.NODE_ENV !== 'development';
console.log('Building Game - is Production:', isProduction, process.env.NODE_ENV);

module.exports = (env) => {
    const config = webpackConfigurator({
        rootDir: __dirname,
        isProduction,
        package,
        DEV_ENV_PROXY,
        isDevEnvProxySecure: true,
        pixiSpineDependency: package.config.pixiSpineDependency,
        fakeBackend: env.fakeBackend,
        lite: env.lite,
        useShellScript: true,
        useDataNextScriptUrl: true,
    });

    const { rules } = config.module;
    rules.push({
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    });
    rules.push({
        test: /\.less$/i,
        use: ['style-loader', 'css-loader', 'less-loader',]
    });
    rules.push({
        test: /\.html$/i,
        loader: 'html-loader'
    });

    return config;

};
